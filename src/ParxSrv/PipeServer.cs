﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.IO.Pipes;
using ParxCli;

using Newtonsoft.Json;



namespace ParxSrv
{

    // Delegate for passing received message back to caller
    public delegate void DelegateMessage(string Reply);

    class PipeServer
    {

        public event DelegateMessage PipeMessage;
        string _pipeName;




        /// <summary>
        /// Create and wait for connexion
        /// </summary>
        /// <param name="PipeName"></param>
        public void Listen(string PipeName)
        {
            try
            {
                // Set to class level var so we can re-use in the async callback method
                _pipeName = PipeName;


                // Pipe security on logued user id
                PipeSecurity ps = new PipeSecurity();
                var usersPipeAccessRule = new PipeAccessRule(
                    new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.BuiltinUsersSid, null),
                    PipeAccessRights.ReadWrite | PipeAccessRights.FullControl,
                    System.Security.AccessControl.AccessControlType.Allow
                    );

                ps.AddAccessRule(usersPipeAccessRule);

                // Create the new async pipe 
                NamedPipeServerStream pipeServer = new NamedPipeServerStream(PipeName, PipeDirection.In, NamedPipeServerStream.MaxAllowedServerInstances, PipeTransmissionMode.Byte, PipeOptions.Asynchronous, 32768, 32768, ps);


                // Wait for a connection
                pipeServer.BeginWaitForConnection (new AsyncCallback(WaitForConnectionCallBack), pipeServer);
            }
            catch (Exception ex)
            {
                Log.add("Listener Error : " + ex.Message, Log.level.srv_error);
            }
        } // end of listen




        /// <summary>
        /// Wait for a new pipe connection
        /// </summary>
        /// <param name="iar">Asynchronous result</param>
        private void WaitForConnectionCallBack(IAsyncResult iar)
        {
            try
            {
                // Get the pipe
                NamedPipeServerStream pipeServer = (NamedPipeServerStream)iar.AsyncState;
                // End waiting for the connection
                pipeServer.EndWaitForConnection(iar);


                byte[] buffer = new byte[65000];

                // Read the incoming message
                pipeServer.Read(buffer, 0, 65000);

                // Convert byte buffer to string
                string stringData = Encoding.UTF8.GetString(buffer, 0, buffer.Length).TrimEnd('\0');
                             

                // Pass message back to calling form
                PipeMessage.Invoke(stringData);



                // run the received task
                runTask(stringData);



                // Kill original sever and create new wait server

                PipeSecurity ps = new PipeSecurity();
                var usersPipeAccessRule = new PipeAccessRule(
                    new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.BuiltinUsersSid, null),
                    PipeAccessRights.ReadWrite | PipeAccessRights.Synchronize,
                    System.Security.AccessControl.AccessControlType.Allow
                    );

                ps.AddAccessRule(usersPipeAccessRule);


                pipeServer.Close();
                pipeServer = null;
                pipeServer = new NamedPipeServerStream(_pipeName, PipeDirection.In, NamedPipeServerStream.MaxAllowedServerInstances, PipeTransmissionMode.Byte, PipeOptions.Asynchronous, 32768, 32768, ps);


                // Recursively wait for the connection again and again...
                pipeServer.BeginWaitForConnection(new AsyncCallback(WaitForConnectionCallBack), pipeServer);
            }
            catch
            {
                return;
            }
        } // end of waitforconnection







        /// <summary>
        /// Run the task send by client
        /// </summary>
        /// <param name="message"></param>
        public void runTask(string message)
        {

            try
            {

                // Parse the message
                dynamic task = Newtonsoft.Json.Linq.JObject.Parse(message);

                Log.add("Task to run : " + task.name, Log.level.srv_info);

                switch ((string)task.name)
                {

                    case "loadCurrentProfileParameters":
                        ParxCli.Classes.ParameterTools.setAllParameters(Newtonsoft.Json.JsonConvert.SerializeObject(task.parameters));
                        break;

                    case "loadPublicDesktop":
                        
                        // delete old shortcuts
                        //ParxCli.Classes.shortcutsTools.clearPublicDesktop(true, false);
                        // load desktop
                        ParxCli.Classes.shortcutsTools.loadPublicDesktop(Newtonsoft.Json.JsonConvert.SerializeObject(task.parameters));
                        break;


                    case "clearPublicDesktop":
                        ParxCli.Classes.shortcutsTools.clearPublicDesktop(true, false);
                        break;

                    case "runAdminTask":

                        string t_name = task.parameters.name;
                        string t_exe = task.parameters.exe;
                        string t_args = task.parameters.args;
                        string t_schedule = task.parameters.schedule;

                        ParxCli.Classes.TaskTools.runAdminTask(t_name, t_exe, t_args, t_schedule);
                        break;


                    case "addSheduledAdminTask":
                        string sht_id = task.id;
                        string sht_name = task.parameters.name;
                        string sht_exe = task.parameters.exe;
                        string sht_args = task.parameters.args;
                        string sht_schedule = task.parameters.schedule;

                        ParxCli.Classes.TaskTools.addSheduledAdminTask(sht_id, sht_name, sht_exe, sht_args, sht_schedule);
                        break;


                    case "deleteSheduledAdminTask":
                        string dsht_id = task.parameters.args;  // id stored in args in delete-schtask

                        ParxCli.Classes.TaskTools.deleteScheduledAdminTask(dsht_id);

                        break;

                    case "updateClient":


                        // run the parx-update scheduled task ?
                        // or let the client do it ?

                        Log.add("starting update service", Log.level.service);

                        try
                        {
                            System.ServiceProcess.ServiceController service = new System.ServiceProcess.ServiceController("Cli2ParxUpd");
                            service.Start();
                        }
                        catch (Exception ex)
                        {
                            Log.add("error with update : " + ex.Message, Log.level.service);
                        }



                        break;


                    default:
                        Log.add("The task [" + task.name + "] is not recognized !", Log.level.task);
                        break;

                }
            }
            catch (Exception ex)
            {
                Log.add("Error in service function runTask with message : " + message + "(" + ex.Message + ")", Log.level.srv_error);
            }
            


        } // end fn runTask



        



    }
}




/*
Console.WriteLine("name : " + task.name);
Console.WriteLine("name : " + task.parameters);

foreach (dynamic p in task.parameters)
{
    Console.WriteLine("-- " + p.name);
}

Console.WriteLine("----------------");
*/




/*
StringBuilder messageBuilder = new StringBuilder();
string messageChunk = string.Empty;
byte[] messageBuffer = new byte[5];
do
{
    pipeServer.Read(messageBuffer, 0, messageBuffer.Length);
    messageChunk = Encoding.UTF8.GetString(messageBuffer);
    messageBuilder.Append(messageChunk);
    messageBuffer = new byte[messageBuffer.Length];
}
while (!pipeServer.IsMessageComplete);

Log.add("Message : " + messageBuilder.ToString() + Environment.NewLine, Log.level.task);

// Pass message back to calling form
PipeMessage.Invoke(messageBuilder.ToString());*/


/* StreamReader reader = new StreamReader(pipeServer);
 Log.add("Gonna read", Log.level.namedpipe, true);

 while (!pipeServer.IsMessageComplete)
 {
     var line = reader.ReadLine();
     Log.add("line : " + line, Log.level.namedpipe, true);

     if (line != "")
     {
         Log.add("Pipe : " + line, Log.level.namedpipe, true);
     }
 }
 */
