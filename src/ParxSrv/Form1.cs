﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ParxCli;



namespace ParxSrv
{
    public partial class Form1 : Form
    {

        public delegate void NewMessageDelegate(string NewMessage);
        private PipeServer _pipeServer;

        public Form1()
        {
            ParxCli.Config config = new Config(@"c:\cli2parx\config.xml");

            Log.add("Config version : " + Config.version, Log.level.srv_debug);


            // Service is running
            Log.add("Service Initialization", Log.level.srv_info);


            // pipe is listening
            try
            {
                _pipeServer = new PipeServer();
                _pipeServer.PipeMessage += new DelegateMessage(PipesMessageHandler);

                _pipeServer.Listen("ParxPipe");
                Log.add("Pipe Initialization", Log.level.srv_info);
            }
            catch (Exception ex)
            {
                Log.add("Pipe INIT Error : " + ex.Message, Log.level.srv_error);
            }

        }


        
        /// <summary>
        /// Message handler
        /// </summary>
        /// <param name="message"></param>
        private void PipesMessageHandler(string message)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new NewMessageDelegate(PipesMessageHandler), message);
                }
                else
                {
                    Log.add("Pipe message : " + message, Log.level.srv_debug);
                }
            }
            catch (Exception ex)
            {
                Log.add("PipeMessageHandler Error: " + ex.Message, Log.level.srv_error);
            }

        }

    }
}




//http://stackoverflow.com/questions/31936100/namedpipeserverstream-receive-max-1024-bytes-why

// creating a service :
// http://stackoverflow.com/questions/593454/easiest-language-for-creating-a-windows-service/593803#593803

// https://gorillacoding.wordpress.com/2013/02/03/using-wcf-for-inter-process-communication/




/*public void StartServer()
 {

     Task.Factory.StartNew(() =>
     {

         PipeSecurity ps = new PipeSecurity();
         System.Security.Principal.SecurityIdentifier sid = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);

         PipeAccessRule par = new PipeAccessRule(sid, PipeAccessRights.ReadWrite, System.Security.AccessControl.AccessControlType.Allow);
         ps.AddAccessRule(par);

         var server = new NamedPipeServerStream("parxPipe", PipeDirection.In, 100, PipeTransmissionMode.Message, PipeOptions.Asynchronous, 32768, 32768, ps);

         try
         {
             server.WaitForConnection();
             Log.add("Wait for connection...", Log.level.namedpipe, true);
         }
         catch (Exception ex)
         {
             Log.add("Pipe ERROR on Wait for connection : " + ex.Message, Log.level.error, true);
         }

StreamReader reader = new StreamReader(server);

        while (!server.IsMessageComplete)
        {
            var line = reader.ReadLine();

            if (line != "")
            {
                Log.add("Pipe : " + line, Log.level.namedpipe, true);
            }

        }
    });
}*/
