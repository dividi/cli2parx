﻿namespace ParxCli
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cLI2ParxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profilsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chargerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bureauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimantesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tachesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sauverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bureauToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterPCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nettoyerLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miseAJourClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rafraichirClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rechargerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.BalloonTipText = "ParxCLI";
            this.notifyIcon.ContextMenuStrip = this.contextMenu;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "ParxCLI";
            this.notifyIcon.Visible = true;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cLI2ParxToolStripMenuItem,
            this.profilsToolStripMenuItem,
            this.chargerToolStripMenuItem,
            this.sauverToolStripMenuItem,
            this.configToolStripMenuItem,
            this.infoToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(120, 158);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            // 
            // cLI2ParxToolStripMenuItem
            // 
            this.cLI2ParxToolStripMenuItem.Enabled = false;
            this.cLI2ParxToolStripMenuItem.Name = "cLI2ParxToolStripMenuItem";
            this.cLI2ParxToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.cLI2ParxToolStripMenuItem.Text = "CLI2Parx";
            // 
            // profilsToolStripMenuItem
            // 
            this.profilsToolStripMenuItem.Name = "profilsToolStripMenuItem";
            this.profilsToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.profilsToolStripMenuItem.Text = "Profils";
            // 
            // chargerToolStripMenuItem
            // 
            this.chargerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toutToolStripMenuItem,
            this.bureauToolStripMenuItem,
            this.partagesToolStripMenuItem,
            this.imprimantesToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.tachesToolStripMenuItem});
            this.chargerToolStripMenuItem.Name = "chargerToolStripMenuItem";
            this.chargerToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.chargerToolStripMenuItem.Text = "Charger";
            // 
            // toutToolStripMenuItem
            // 
            this.toutToolStripMenuItem.Name = "toutToolStripMenuItem";
            this.toutToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.toutToolStripMenuItem.Text = "Tout";
            this.toutToolStripMenuItem.Click += new System.EventHandler(this.toutToolStripMenuItem_Click);
            // 
            // bureauToolStripMenuItem
            // 
            this.bureauToolStripMenuItem.Name = "bureauToolStripMenuItem";
            this.bureauToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.bureauToolStripMenuItem.Text = "Bureau";
            this.bureauToolStripMenuItem.Click += new System.EventHandler(this.bureauToolStripMenuItem_Click);
            // 
            // partagesToolStripMenuItem
            // 
            this.partagesToolStripMenuItem.Name = "partagesToolStripMenuItem";
            this.partagesToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.partagesToolStripMenuItem.Text = "Partages";
            this.partagesToolStripMenuItem.Click += new System.EventHandler(this.partagesToolStripMenuItem_Click);
            // 
            // imprimantesToolStripMenuItem
            // 
            this.imprimantesToolStripMenuItem.Name = "imprimantesToolStripMenuItem";
            this.imprimantesToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.imprimantesToolStripMenuItem.Text = "Imprimantes";
            this.imprimantesToolStripMenuItem.Click += new System.EventHandler(this.imprimantesToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // tachesToolStripMenuItem
            // 
            this.tachesToolStripMenuItem.Name = "tachesToolStripMenuItem";
            this.tachesToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.tachesToolStripMenuItem.Text = "Tâches";
            this.tachesToolStripMenuItem.Click += new System.EventHandler(this.tachesToolStripMenuItem_Click);
            // 
            // sauverToolStripMenuItem
            // 
            this.sauverToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bureauToolStripMenuItem1});
            this.sauverToolStripMenuItem.Name = "sauverToolStripMenuItem";
            this.sauverToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.sauverToolStripMenuItem.Text = "Sauver";
            // 
            // bureauToolStripMenuItem1
            // 
            this.bureauToolStripMenuItem1.Name = "bureauToolStripMenuItem1";
            this.bureauToolStripMenuItem1.Size = new System.Drawing.Size(111, 22);
            this.bureauToolStripMenuItem1.Text = "Bureau";
            this.bureauToolStripMenuItem1.Click += new System.EventHandler(this.bureauToolStripMenuItem1_Click);
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajouterPCToolStripMenuItem,
            this.nettoyerLogToolStripMenuItem,
            this.miseAJourClientToolStripMenuItem,
            this.rafraichirClientToolStripMenuItem});
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.configToolStripMenuItem.Text = "Config";
            // 
            // ajouterPCToolStripMenuItem
            // 
            this.ajouterPCToolStripMenuItem.Name = "ajouterPCToolStripMenuItem";
            this.ajouterPCToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.ajouterPCToolStripMenuItem.Text = "Ajouter PC";
            // 
            // nettoyerLogToolStripMenuItem
            // 
            this.nettoyerLogToolStripMenuItem.Name = "nettoyerLogToolStripMenuItem";
            this.nettoyerLogToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.nettoyerLogToolStripMenuItem.Text = "Nettoyer Log";
            this.nettoyerLogToolStripMenuItem.Click += new System.EventHandler(this.nettoyerLogToolStripMenuItem_Click);
            // 
            // miseAJourClientToolStripMenuItem
            // 
            this.miseAJourClientToolStripMenuItem.Name = "miseAJourClientToolStripMenuItem";
            this.miseAJourClientToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.miseAJourClientToolStripMenuItem.Text = "Mise à jour Client";
            this.miseAJourClientToolStripMenuItem.Click += new System.EventHandler(this.miseAJourClientToolStripMenuItem_Click);
            // 
            // rafraichirClientToolStripMenuItem
            // 
            this.rafraichirClientToolStripMenuItem.Name = "rafraichirClientToolStripMenuItem";
            this.rafraichirClientToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.rafraichirClientToolStripMenuItem.Text = "Rafraichir Client";
            this.rafraichirClientToolStripMenuItem.Click += new System.EventHandler(this.rafraichirClientToolStripMenuItem_Click);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logToolStripMenuItem});
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.infoToolStripMenuItem.Text = "Info";
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size(94, 22);
            this.logToolStripMenuItem.Text = "Log";
            this.logToolStripMenuItem.Click += new System.EventHandler(this.logToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitterToolStripMenuItem1,
            this.rechargerToolStripMenuItem});
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem1
            // 
            this.quitterToolStripMenuItem1.Name = "quitterToolStripMenuItem1";
            this.quitterToolStripMenuItem1.Size = new System.Drawing.Size(127, 22);
            this.quitterToolStripMenuItem1.Text = "Quitter";
            this.quitterToolStripMenuItem1.Click += new System.EventHandler(this.quitterToolStripMenuItem1_Click);
            // 
            // rechargerToolStripMenuItem
            // 
            this.rechargerToolStripMenuItem.Name = "rechargerToolStripMenuItem";
            this.rechargerToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.rechargerToolStripMenuItem.Text = "Recharger";
            this.rechargerToolStripMenuItem.Click += new System.EventHandler(this.rechargerToolStripMenuItem_Click);
            // 
            // Timer
            // 
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // Form1
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(15, 15);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(-15, -15);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Parx CLI2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem cLI2ParxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profilsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chargerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sauverToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bureauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem partagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimantesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tachesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bureauToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ajouterPCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nettoyerLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rechargerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miseAJourClientToolStripMenuItem;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.ToolStripMenuItem rafraichirClientToolStripMenuItem;
    }
}

