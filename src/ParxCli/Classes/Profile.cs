﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParxCli.Classes
{
    class Profile
    {

        public int id;
        public string name;
        
        public List<Shortcut> shortcuts;
        public List<Share> shares;
        public List<Printer> printers;
        public List<Parameter> parameters;


        // load desktop + shares + ...
        public void loadProfile() {


        }




        /// <summary>
        /// load on the USER desktop
        /// </summary>
        public void loadUserDesktop ()
        {
            Log.add("Writing Shortcuts on USER desktop", Log.level.info);

            foreach (Shortcut shortcut in shortcuts)
            {
                // Only shortcuts of the desktop
                if (shortcut.location == 0)
                {
                    shortcut.create(Shortcut.desktop.user);
                }

            }

        }



        /// <summary>
        /// Draw on the PUBLIC desktop
        /// </summary>
        public void loadPublicDesktop()
        {
            // new pipe            
            Pipe _pipeClient = new Pipe();

            // make a task object
            Object obj = new
            {
                name = "loadPublicDesktop",
                id = 0,
                parameters = this.shortcuts
            };

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None);

            _pipeClient.Send(json);

        }




        /// <summary>
        /// Create shortcuts on desktop (taskbar, shortcuts and clearing functions are wrapped)
        /// </summary>
        /// <param name="whitchDesktop">user or public desktop</param>
        /// <param name="deleteFiles">Delete all files (not only shortcuts)</param>
        /// <param name="onlyParxShortcuts">Delete only Parx tagged shortcuts</param>
        public void loadDesktop(string whitchDesktop, bool deleteFiles, bool onlyParxShortcuts, bool cleanUserDesktop, bool cleanPublicDesktop) {


            // clear the taskbar
            Host.clearTaskbar();

            // load taskbar
            this.loadTaskbar();


            // delete USER shortcuts ?
            if (cleanUserDesktop)
            {
                Host.clearUserDesktop(deleteFiles, onlyParxShortcuts);
            }
            
            // delete PUBLIC shortcuts ?
            if (cleanPublicDesktop)
            {
                Pipe.sendJsonToPipe("0", "clearPublicDesktop", "");
            }


            // write in the USER desktop
            if (whitchDesktop == "user")
            {               
                this.loadUserDesktop();
            }

            // write in PUBLIC desktop
            if (whitchDesktop == "public")
            {
                this.loadPublicDesktop();
            }


        }





        /// <summary>
        /// Create shortcuts on taskbar
        /// </summary>
        public void loadTaskbar()
        {

            Log.add("Writing taskbar", Log.level.info);

            foreach (Shortcut shortcut in this.shortcuts)
            {
                // Only shortcuts of the taskbar
                if (shortcut.location == 1)
                {
                    shortcut.pinToTaskbar();
                }
            }

        }



        /// <summary>
        /// Mount of the current profile
        /// </summary>
        /// <param name="clearShares">unmount all shares (except U) before mounting</param>
        public void loadShares (bool clearShares)
        {
            Log.add("Adding shares", Log.level.info);

            if (clearShares)
            {
                Host.clearShares();
            }
            

            foreach(Share share in shares)
            {
                share.mount();
            }
        }



        /// <summary>
        /// Mount printers of the current profile
        /// </summary>
        /// <param name="clearPrinters"></param>
        public void loadPrinters(bool clearPrinters)
        {
            Log.add("Mount Printers", Log.level.info);

            if (clearPrinters)
            {
                Host.clearPrinters();
            }


            foreach (Printer printer in printers)
            {
                printer.mount();
            }
        }



        /// <summary>
        /// Load the parameters of the profile. No need anymore
        /// </summary>
        public void loadParameter()
        {

            Log.add("Load parameters", Log.level.info);

            foreach (Parameter parameter in parameters)
            {
                parameter.setParameter();
            }

        }




    }
}


