﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using IWshRuntimeLibrary;

namespace ParxCli.Classes
{
    class Shortcut
    {
        
        public int id;
        public string name;
        public string target;
        public string arguments;
        public string workingDir;
        public string description;
        public int location;

        public enum desktop {
            allusers, user
        };

        

        /// <summary>
        /// Constructor : create a new shortcut object
        /// </summary>
        public Shortcut (string name, string target, string arguments, string workingDir, string description, int location)
        {
            this.name = name;
            this.target = target;
            this.arguments = arguments;
            this.workingDir = workingDir;
            this.description = description;
            this.location = location;
        }



        /// <summary>
        /// Create a shortcut on a specific desktop
        /// </summary>
        /// <param name="whitchDesktop">Select the desktop to write on</param>
        public void create(desktop whitchDesktop)
        {

            WshShell shell = new WshShell();
            
            // where to create the shortcut
            string shortcutLocation = whitchDesktop == desktop.user ? System.IO.Path.Combine(Config.getUserDesktopPath(), this.name + ".lnk") : System.IO.Path.Combine(Config.getPublicDesktopPath(), this.name + ".lnk");

            try
            {

                if ( System.IO.File.Exists(this.target) )
                {

                    IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutLocation);

                    shortcut.Description = this.description + " [PARX]";    // The description of the shortcut
                                                                            // shortcut.IconLocation = @"c:\myicon.ico";             // The icon of the shortcut
                    shortcut.TargetPath = this.target;                      // The path of the file that will launch when the shortcut is run
                    shortcut.Arguments = this.arguments;                    // Arguments
                    shortcut.WorkingDirectory = this.workingDir;            // working dir
                    shortcut.Save();                                        // Save the shortcut

                    Log.add("-- Create shortcut on " + whitchDesktop.ToString() + " : " + this.name, Log.level.trivial);

                }

                else
                {
                    Log.add("-- No target for shortcut " + this.name, Log.level.trivial);
                }


            }
            catch (Exception ex) {
                Log.add(ex.Message, Log.level.error);
            }


        }





        /// <summary>
        /// Pin a shortcut to taskbar with pinTo10 (https://pinto10blog.wordpress.com/2016/09/10/pinto10/)
        /// </summary>
        public void pinToTaskbar ()
        {
            try
            {

                // folder is writable or throw an error !
                //System.Security.AccessControl.DirectorySecurity ds = Directory.GetAccessControl(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar");
                
                var proc = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo
                    {
                        FileName = AppDomain.CurrentDomain.BaseDirectory + "PinTo10v2.exe",
                        Arguments = "/pintb \"" + this.target + "\"",
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                proc.Start();

                while (!proc.StandardOutput.EndOfStream) 
                { 
                   string line = proc.StandardOutput.ReadLine(); 
                   Log.add("-- try to pin : " + proc.StartInfo.Arguments, Log.level.debug); 
                   Log.add("-- XXXXXXXXx : " + line, Log.level.debug); 
                }

                Log.add("-- Pin shortcut : " + this.name, Log.level.trivial);
            }
            catch (Exception ex)
            {
                Log.add("Error adding taskbar pin (" + this.name + ") : " + ex.Message, Log.level.warning);
            }
        }


    }






    /// <summary>
    /// Some tools to work with the service (it must be shitty but ey, I'm just human !)
    /// </summary>
    public class shortcutsTools
    {


        /// <summary>
        /// Draw shortcuts from a service task
        /// </summary>
        /// <param name="json">A serialized List(Shortcut) </param>
        static public void loadPublicDesktop(string json)
        {
           
            try
            {
                // deserialize the message
                List<Shortcut> shortcuts = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Shortcut>>(json);

                foreach (Shortcut shortcut in shortcuts)
                {
                    // Only shortcuts of the desktop
                    if (shortcut.location == 0)
                    {
                        shortcut.create(Shortcut.desktop.allusers);
                    }
                }
            }
            catch (Exception ex)
            {

                Log.add("Error in loadPublicDesktop function : " + ex.Message + "(" + ex.StackTrace + ")", Log.level.debug);
            }
            
        }


        
        /// <summary>
        /// Clear the public desktop
        /// </summary>
        /// <param name="deleteFiles">Delete shortcuts AND files, folders...</param>
        /// <param name="onlyParxShortcuts">Delete only Parx made shortcuts</param>
        static public void clearPublicDesktop(bool deleteFiles, bool onlyParxShortcuts)
        {

            Log.add("Clear PUBLIC desktop", Log.level.info);

            // If delete all files (also delete PARX shortcuts)
            if (deleteFiles)
            {

                Log.add("Delete files", Log.level.info);

                DirectoryInfo di = new DirectoryInfo(Config.getPublicDesktopPath());

                // delete all files
                foreach (FileInfo file in di.GetFiles())
                {
                    Log.add("-- Delete file : " + file.Name, Log.level.trivial);
                    file.Delete();
                }

                // delete all folders
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    Log.add("-- Delete folder : " + dir.FullName, Log.level.trivial);
                    dir.Delete(true);
                }


            }

            // If delete only shortcuts
            else
            {
                var files = Directory.EnumerateFiles(Config.getPublicDesktopPath(), "*.lnk");

                // delete only shortcuts with [PARX]
                if (onlyParxShortcuts)
                {

                    Log.add("Delete PARX shortcuts", Log.level.info);

                    WshShell shell = new WshShell();

                    foreach (var file in files)
                    {

                        string description = shell.CreateShortcut(file).Description;

                        if (description.Contains("[PARX]"))
                        {
                            Log.add("-- Delete shortcut : " + file, Log.level.trivial);
                            System.IO.File.Delete(file);
                        }

                    }

                }

                // delete all shortcuts
                else
                {
                    Log.add("Delete ALL shortcuts", Log.level.info);

                    foreach (var file in files)
                    {
                        Log.add("-- Delete file : " + file, Log.level.trivial);
                        System.IO.File.Delete(file);
                    }
                }


            }

        }




    }





}




