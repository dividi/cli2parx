﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.IO;

using System.Net.Http;
using System.Net.Http.Headers;

using Newtonsoft.Json; 

using RestSharp;    // http://restsharp.org/  and  http://pawel.sawicz.eu/restsharp/

using ParxCli.Classes;




// deserializ JSON : http://stackoverflow.com/questions/6620165/how-can-i-parse-json-with-c


namespace ParxCli
{
    class Rest
    {

        // members
        private string _URI;
        private RestClient _client;

        // constructor (set base URI)
        public Rest(string url)
        {
            this._URI = url + "/";
            this._client = new RestClient(_URI);

            // maybe a way to bypass the proxy cache/auth
            //HttpWebRequest.DefaultWebProxy = null;
        }



        /// <summary>
        /// Get host ID by giving the host name
        /// </summary>
        /// <param name="hostname"></param>
        /// <returns>host ID</returns>
        public string getHostID(string hostname) {

            try
            {
                // the request
                var request = new RestRequest("rest_hostid/" + hostname, Method.GET);

                request.RequestFormat = DataFormat.Json;

                // get the result
                var queryResult = _client.Execute(request);

                // response must be OK
                if (queryResult.StatusCode == HttpStatusCode.OK)
                {
                    dynamic response = Newtonsoft.Json.Linq.JObject.Parse(queryResult.Content);

                    if (response.code == "1")
                    {
                        return null;
                    }
                    else
                    {
                        return response.message;
                    }
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                Log.add("ERROR in REST response : " + ex.Message, Log.level.error);
            }

            return null;
            
        }


        
        /// <summary>
        /// Add the host in the database
        /// </summary>
        /// <param name="data"></param>
        public void addHost(object data)
        {
            try
            {
                var request = new RestRequest("rest_host", Method.POST);

                request.RequestFormat = DataFormat.Json;

                request.AddBody(data);

                var response = _client.Execute(request);
           
                Log.add("Host " + data.GetType().GetProperty("hostName").GetValue(data) + " added in database", Log.level.info);
            }
            catch (Exception ex)
            {
                Log.add("ERROR in REST response : " + ex.Message, Log.level.error);
            }
        }



        /// <summary>
        /// Update the host in the database (version + OS)
        /// </summary>
        /// <param name="data"></param>
        public void updateHost(object data)
        {
            try { 
                var request = new RestRequest("rest_hostupdate", Method.POST);

                request.RequestFormat = DataFormat.Json;

                request.AddBody(data);

                var response = _client.Execute(request);

                Log.add("Host " + data.GetType().GetProperty("hostName").GetValue(data) + " updated in database", Log.level.info);
            }
            catch (Exception ex)
            {
                Log.add("ERROR in REST response : " + ex.Message, Log.level.error);
            }
}



        /// <summary>
        /// get matching profiles
        /// </summary>
        /// <param name="username"></param>
        /// <param name="hostname"></param>
        /// <returns></returns>
        public List<Profile> getProfilesList(string username, string hostname) {

            List<Profile> profilesList = new List<Profile>();

            try
            {

                // the request
                var request = new RestRequest("REST_fullProfiles/" + hostname + "/" + username, Method.GET);

                request.RequestFormat = DataFormat.Json;

                // get the result
                var queryResult = _client.Execute(request);

                // response must be OK
                if (queryResult.StatusCode == HttpStatusCode.OK)
                {
                    var json = (string)queryResult.Content;

                    // deserialize the json string in Profile object
                    try
                    {
                        Log.add("Deserialize Json string : " + json, Log.level.debug);
                        var JsonObj = JsonConvert.DeserializeObject<List<Profile>>(json);
                        return JsonObj;
                    }
                    catch (Exception ex)
                    {
                        Log.add("Error with JSON deserialization (Connection error ?) : " + ex.Message, Log.level.error);
                    }
                }

            }
            catch (Exception general_ex)
            {
                Log.add("ERROR in REST response : " + general_ex.Message, Log.level.error);
            }



            return null;

        }



        /// <summary>
        /// get tasks for a host
        /// </summary>
        /// <param name="hostid">Id of host</param>
        /// <returns></returns>
        public List<Classes.Task> getTasksList(string hostid)
        {

            List<Classes.Task> taskList = new List<Classes.Task>();

            try
            {
                // the request
                var request = new RestRequest("rest_tasks/" + hostid, Method.GET);

                request.RequestFormat = DataFormat.Json;

                // get the result
                var queryResult = _client.Execute(request);

                // response must be OK
                if (queryResult.StatusCode == HttpStatusCode.OK)
                {
                    var json = (string)queryResult.Content;


                    dynamic response = Newtonsoft.Json.Linq.JObject.Parse(json);

                    if (response.code == "0")
                    {
                        // deserialize the json string in Task object
                        try
                        {
                            Log.add("Deserialize Json string : " + json, Log.level.debug);
                            var JsonObj = JsonConvert.DeserializeObject<List<Classes.Task>>((string)response.message);
                            return JsonObj;
                        }
                        catch (Exception ex)
                        {
                            Log.add("Error with JSON deserialization (Connection error ?) : " + ex.Message, Log.level.error);
                        }
                    } // if response is 0
                } // if result is ok
            }
            catch (Exception general_ex)
            {
                Log.add("ERROR in REST response : " + general_ex.Message, Log.level.error);
            }
            return null;

        }


        /// <summary>
        /// Send a list of shortcuts to the server
        /// </summary>
        /// <param name="profileID">Pofile ID to affect the shortcuts to</param>
        /// <param name="shortcutsList">the shortcut to post</param>
        public void sendShortcutsList(int profileID, List<Shortcut> shortcutsList)
        {
            try { 
                var request = new RestRequest("REST_saveShortcuts", Method.POST);

                request.RequestFormat = DataFormat.Json;

                string output = JsonConvert.SerializeObject(shortcutsList);

                var data = new { profileId = profileID, desktop = output };

                request.AddBody(data);

                var response = _client.Execute(request);

                Log.add("response sendShortcutsList: " + response.Content, Log.level.debug);
            }
            catch (Exception general_ex)
            {
                Log.add("ERROR in REST response : " + general_ex.Message, Log.level.error);
            }
}




        /// <summary>
        /// Send the version tag (in config.xml) to monitor it on server host page
        /// </summary>
        /// <param name="hostname">The host name</param>
        /// <param name="version">The client version tag set in config.xml</param>
        public void sendHostVersion(string hostname, string version) {
            try { 
                var request = new RestRequest("rest_version", Method.POST);

                request.RequestFormat = DataFormat.Json;

                var data = new { hostName = hostname, version = version };

                request.AddBody(data);

                var response = _client.Execute(request);

                Log.add("Update version to " + version + " for host " + hostname, Log.level.trivial);

                Log.add("response sendHostVersion: " + response.Content, Log.level.debug);
            }
            catch (Exception general_ex)
            {
                Log.add("ERROR in REST response : " + general_ex.Message, Log.level.error);
            }

        }





        // @ TODO : request is GET and must be POST, should be fixed in server part (RestController.php, function delTask) !
        /// <summary>
        /// Delete a task by ID
        /// </summary>
        /// <param name="taskID"></param>
        public void deleteTask(string task_id)
        {
            try
            {
                Log.add("Delete task " + task_id + " in server", Log.level.debug);

                // request
                var request = new RestRequest("rest_deltask/" + task_id, Method.POST);

                request.RequestFormat = DataFormat.Json;

                var data = new { taskid = task_id };

                request.AddBody(data);

                

                // get the result
                var queryResult = _client.Execute(request);
            }
            catch (Exception ex)
            {
                Log.add("Error in deleteTask REST function : " + ex.Message, Log.level.error);
            }
            
            
        }

    }






} // end o' class
