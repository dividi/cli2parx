﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IWshRuntimeLibrary;


namespace ParxCli.Classes
{

    class PrinterPivot
    {
        public string Default;    // Default with a captital (default is a reserved word)

        public PrinterPivot(string defaultPrinter)
        {
            // this.Default = defaultPrinter == "1" ? true : false;
            this.Default = defaultPrinter;
        }
    }




    class Printer
    {
        public string name;
        public string target;
        //public bool defaultPrinter;
        public string defaultPrinter;


        public Printer (string name, string target, PrinterPivot pivot)
        {
            this.name = name;
            this.target = target;
            this.defaultPrinter = pivot.Default;
        }


    
        
        /// <summary>
        /// Mount a printer
        /// </summary>
        public void mount()
        {
            try
            {
                WshNetwork objNetwork = new WshNetwork();
                WshNetwork oNetwork = new WshNetwork();

                // mount
                oNetwork.AddWindowsPrinterConnection(this.target, this.name, this.target);

                // set default printer
                //if ( this.defaultPrinter == true )
                if (this.defaultPrinter == "1")
                {
                    Log.add("-- Mount printer : " + this.name + " on " + this.target + " (default)", Log.level.trivial);

                    oNetwork.SetDefaultPrinter(this.target);
                }
                else
                {
                    Log.add("-- Mount printer : " + this.name + " on " + this.target, Log.level.trivial);
                }

                
            }
            catch (Exception ex)
            {
                Log.add("Error while mounting " + this.name + " (" + ex.Message + ")", Log.level.error);
            }
            

        }
        

    }
}
