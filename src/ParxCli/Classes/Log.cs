﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace ParxCli
{
   public class Log
    {

        static private string _logFile = Config.logFile;

        // Limit to 10 chars please
        public enum level {
            trivial, error, warning, info, task, pipe, debug, service, srv_error, srv_info, srv_debug, reg_error
        }



        static private bool allowLog (level type)
        {

            switch (type)
            {
                case level.trivial:
                    if (Config.logTrivial) return true; break;

                case level.error:
                    if (Config.logErrors) return true; break;

                case level.warning:
                    if (Config.logWarns) return true; break;

                case level.info:
                    if (Config.logInfo) return true; break;

                case level.task:
                    if (Config.logTasks) return true; break;

                case level.pipe:
                    if (Config.logInfo) return true; break;
                    
                case level.debug:
                    if (Config.logDebug) return true; break;

                case level.service:
                    if (Config.logInfo) return true; break;

                case level.srv_error:
                    if (Config.logErrors) return true; break;

                case level.srv_info:
                    if (Config.logInfo) return true; break;

                case level.srv_debug:
                    if (Config.logDebug) return true; break;

                case level.reg_error:
                    if (Config.logErrors) return true; break;

                default: return false;

            }

            return false;

        }


        // @TODO : test if type.tostring > 10

        /// <summary>
        /// Add a new log
        /// </summary>
        /// <param name="value">The string to add</param>
        /// <param name="type">the log level</param>
        static public void add( string value, level type) {

            if (allowLog(type))
            {
                
                string message = "";
                string now = DateTime.Now.ToString("yyyyMMdd@HH:mm");

                // set a nice log level
                string numberOfSpaces = "";

                for (int i = 0; i <= 10 - type.ToString().Length; i++)
                {
                    numberOfSpaces += " ";
                }

                message = now + " [" + type.ToString().ToUpper() + numberOfSpaces + "] " + value;


                try { 

                    // log to windows log events
                    if ( Config.logToEvents == true )
                    {
                        string sSource = "Parx";
                        string sLog = "Application";
                        string sEvent = "[" + type.ToString().ToUpper() + numberOfSpaces + "] " + value;

                        if (!EventLog.SourceExists(sSource))
                            EventLog.CreateEventSource(sSource, sLog);

                        // errors
                        if ( type == level.error || type == level.reg_error || type == level.srv_error )
                        {
                            EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Error, 58008);
                        }

                        // warning
                        else if (type == level.warning)
                        {
                            EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Warning, 1337);
                        }

                        // info
                        else
                        {
                            EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Information, 42);
                        }
                    }



                    // log to file
                    if (Config.logToFile == true)
                    {
                        StreamWriter writer = new StreamWriter(_logFile, true, Encoding.UTF8);
                        writer.WriteLine(message);
                        writer.Dispose();
                    }


                }
                catch (Exception ex)
                {

                    // log in windows log events
                    string sSource = "Parx";
                    string sLog = "Application";
                    string sEvent = "[ERROR      ] Log write error : " + ex.Message;

                    if (!EventLog.SourceExists(sSource))
                        EventLog.CreateEventSource(sSource, sLog);

                    EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Error, 58008);



                    // stupid : if it cannnot write, I write the exception ???!
                    StreamWriter writer = new StreamWriter(_logFile, true, Encoding.UTF8);
                    message = "[ERROR      ] Log write error : " + ex.Message;
                    writer.WriteLine(message);
                    writer.Dispose();

                }
            }

        }





        /// <summary>
        /// clear the log file
        /// </summary>
        static public void clearLogFile() {
            try
            {
                File.WriteAllText(_logFile, string.Empty);
                add("Clearing the Log file", level.info);
            }
            catch (Exception ex)
            {
                add("Error cleaning the log file : " + ex.Message, level.error);
            }     
        }
     

    }
}
