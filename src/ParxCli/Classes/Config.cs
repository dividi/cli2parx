﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml.Linq;
using System.Management;

namespace ParxCli
{



    /// <summary>
    /// parse XML config file and set variables (hostname, user name...)
    /// </summary>
    public class Config
    {

        // CONFIG
        static public string version;                   // version number
        static public string serverURI;                 // serveur URI for REST
        static public string logFile;                   // log file path (relative / absolute) (WARN : static for now)
        static public bool logClearOnStart;             // log clear on startup
        static public bool autoRegisterHost;            // auto register host in server
        static public bool deleteDesktopFiles;          // delete files on desktop (not only shortcuts)

        static public bool cleanUserDesktop;            // clean user shortcuts
        static public bool cleanPublicDesktop;          // clean public shortcuts

        static public bool deleteOnlyParxShortcuts;     // delete only Parx made shortcuts
        static public string whichDesktop;              // desktop to write on shortcuts
        static public bool unmountShares;               // unmount shares before mounting them again
        static public bool unmountPrinters;             // unmount printers before mounting them again

        static public bool disableMenus;                // disable some menus
        static public bool kickWithoutProfile;          // kick user without profile

        static public bool logToFile;       // log to log file
        static public bool logToEvents;     // log to windows log events

        static public bool logErrors;   // log errors
        static public bool logWarns;    // log warnings
        static public bool logInfo;     // log info
        static public bool logTasks;    // log tasks
        static public bool logTrivial;  // log trivial
        static public bool logDebug;    // log debug

        static public bool autoShortcuts;    // autoload shortcuts
        static public bool autoPrinters;    // autoload printers
        static public bool autoShares;    // autoload shares
        static public bool autoParameters;    // autoload parameters
        static public bool autoTasks;    // autoload tasks

        static public string[] adminList;     // client admin list

        static public string refreshRate;   // task refresh rate


        // VARIABLES
        static public string username;         // user name
        static public string loguedUsername;   // logued user name
        static public string hostname;         // host name
        static public string hostOS;           // host OS
        static public string MSID;           // user MSID




        /// <summary>
        /// Load XML and set values
        /// </summary>
        /// <param name="XmlConfigFile">The config XML file</param>
        public Config(string XmlConfigFile) {
            
            XDocument doc = XDocument.Load(XmlConfigFile);

            // config
            version = getString(doc, "version");
            serverURI = getString(doc, "serverURL");
            logFile = getString(doc, "logFile");
            logClearOnStart = getBool(doc, "logClearOnStart");
            autoRegisterHost = getBool(doc, "autoRegisterHost");
            deleteDesktopFiles = getBool(doc, "deleteDesktopFiles");
            deleteOnlyParxShortcuts = getBool(doc, "deleteOnlyParxShortcuts");

            cleanUserDesktop = getBool(doc, "cleanUserDesktop");
            cleanPublicDesktop = getBool(doc, "cleanPublicDesktop");

            whichDesktop = getString(doc, "whichDesktop");
            
            logToFile = getBool(doc, "logToFile");
            logToEvents = getBool(doc, "logToEvents");

            logErrors = getBool(doc, "logErrors");
            logWarns = getBool(doc, "logWarns");
            logInfo = getBool(doc, "logInfo");
            logTasks = getBool(doc, "logTasks");
            logTrivial = getBool(doc, "logTrivial");
            logDebug = getBool(doc, "logDebug");

            disableMenus = getBool(doc, "disableMenus");
            kickWithoutProfile = getBool(doc, "kickWithoutProfile");

            unmountShares = getBool(doc, "unmountShares");
            unmountPrinters = getBool(doc, "unmountPrinters");

            autoShortcuts = getBool(doc, "autoShortcuts");
            autoPrinters = getBool(doc, "autoPrinters");
            autoShares = getBool(doc, "autoShares");
            autoParameters = getBool(doc, "autoParameters");
            autoTasks = getBool(doc, "autoTasks");

            adminList = Array.ConvertAll(getString(doc, "adminList").Split(','), p => p.Trim());

            refreshRate = getString(doc, "refreshRate");

            // variables
            username = this.getUsername();
            loguedUsername = getLoguedUsername();
            hostname = this.getHostname();
            hostOS = this.getHostOS();
            MSID = getUserMSID(username);

        }



        
        /// <summary>
        /// get the field value of a XML doc
        /// </summary>
        /// <param name="doc">the XDocument</param>
        /// <param name="field">name of the field</param>
        /// <returns></returns>
        private string getString(XDocument doc, string field)
        {
            try
            {
                return doc.Descendants(field).Single<XElement>().Value;
            }
            catch(Exception ex)
            {
                Log.add("Error on a Config variable : " + field + "(" + ex.Message + ")", Log.level.error);
                return "";
            }
            
        }

        

        /// <summary>
        /// get the field value of a XML doc
        /// </summary>
        /// <param name="doc">the XDocument</param>
        /// <param name="field">name of the field</param>
        /// <returns></returns>
        private bool getBool(XDocument doc, string field)
        {
            try
            {
                return doc.Descendants(field).Single<XElement>().Value == "true" ? true : false;
            }
            catch (Exception ex)
            {
                Log.add("Error on a Config variable : " + field + "(" + ex.Message + ")", Log.level.error);
                return false;
            }

        }


        /// <summary>
        /// get user name (the current one)
        /// </summary>
        /// <returns>user name</returns>
        private string getUsername() {
            return Environment.UserName;
        }



        /// <summary>
        /// get the logued user (domain + username) or username (could be a problem)
        /// </summary>
        /// <returns></returns>
        static public string getLoguedUsername() {
            //return System.Security.Principal.WindowsIdentity.GetCurrent().Name;  // logued user

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT UserName FROM Win32_ComputerSystem");
            ManagementObjectCollection collection = searcher.Get();
            string usernameWithDomain = (string)collection.Cast<ManagementBaseObject>().First()["UserName"];

            if (usernameWithDomain != null)
            {
                string[] username = usernameWithDomain.Split(new char[] { '\\' });

                if (username[1] != null)
                    return username[1];
                else
                    return usernameWithDomain;
            }
            else
            {
                return Environment.UserName;
            }
        }

        /*
        /// <summary>
        /// get the logued user (domain + username) or username (could be a problem)
        /// </summary>
        /// <returns></returns>
        static public string getLogUsername()
        {
            //return System.Security.Principal.WindowsIdentity.GetCurrent().Name;  // logued user

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT UserName FROM Win32_ComputerSystem");
            ManagementObjectCollection collection = searcher.Get();
            string usernameWithDomain = (string)collection.Cast<ManagementBaseObject>().First()["UserName"];

            if (usernameWithDomain != null)
            {
                string[] username = usernameWithDomain.Split(new char[] { '\\' });

                if (username[1] != null)
                    return username[1];
                else
                    return usernameWithDomain;
            }
            else
            {
                return Environment.UserName;
            }
        }*/



        /// <summary>
        /// get host name
        /// </summary>
        /// <returns>Host Name</returns>
        private string getHostname() {
            return Environment.MachineName;
        }

        

        /// <summary>
        /// get host OS (human readable)
        /// </summary>
        /// <returns>host OS</returns>
        private string getHostOS() {
            var name = (from x in new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem").Get().Cast<ManagementObject>()
                        select x.GetPropertyValue("Caption")).FirstOrDefault();
            return name != null ? name.ToString() : "Unknown";
        }
        


        /// <summary>
        /// Get user desktop path
        /// </summary>
        /// <returns>user desktop path</returns>
        static public string getUserDesktopPath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        }

        

        /// <summary>
        /// get public desktop path
        /// </summary>
        /// <returns>public desktop path</returns>
        static public string getPublicDesktopPath()
        {
            return "C:\\Users\\Public\\Desktop";
        }



        /// <summary>
        /// get user MSID (to write in HKU)
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        static public string getUserMSID (string username)
        {
            //$sid = Get-WmiObject -Class Win32_UserAccount -Filter "name='$userName'" | select SID

            ManagementObjectSearcher objSearcher = new ManagementObjectSearcher(@"SELECT SID FROM Win32_UserAccount WHERE name='" + username + "'");
            ManagementObjectCollection objCollection = objSearcher.Get();

           // Console.WriteLine("obj : " + objCollection[0]["SID"]);

            foreach (var obj in objCollection)
            {
                return "" + obj["SID"];
            }

            return "";
        }




    }
}
