﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Management;

namespace ParxCli.Classes
{

    /// <summary>
    /// Pivot class to json 
    /// </summary>
    public class ParameterPivot
    {
        public string parameterValue;

        public ParameterPivot(string parameterValue)
        {
            this.parameterValue = parameterValue;
        }
    }



    public class Parameter
    {

        public string name;
        public string defaultValue;
        public string value;


        public Parameter(string name, string defaultValue, ParameterPivot pivot)
        {
            this.name = name;
            this.defaultValue = defaultValue;
            this.value = pivot.parameterValue;
        }



        // not really useful
        public void setParameter()
        {

            /*switch (this.name)
            {
               // case "proxyUrlIE": ParameterTools.setSystemProxyUrl(this.value); break;
               // case "hideControlPanel": ParameterTools.hideControlPanel(this.value); break;
            }*/

        }


    }


    public class ParameterTools
    {


        /// <summary>
        /// Set all parameters
        /// </summary>
        /// <param name="jsonParameters"></param>
        static public void setAllParameters(string jsonParameters)
        {

            Log.add("Setting All Parameters : " + jsonParameters, Log.level.debug);

            try
            {

                // Init (these two )
                string msid = Config.getUserMSID(Config.getLoguedUsername());
                RegTools.CreateRegistrySubKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion", "Policies");
                RegTools.CreateRegistrySubKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Policies", "Explorer");

                // deserialize json Parameters
                dynamic list = Newtonsoft.Json.JsonConvert.DeserializeObject(jsonParameters);

                // init firefox
                if ( firefoxIsInstalled() == true )
                {
                    initFirefoxFiles();
                }

                foreach (dynamic p in list)
                {

                    string paramName = p.name;
                    string paramValue = p.value;

                    //Log.add("******* param : " + paramName + ", value : " + paramValue, Log.level.debug);


                    switch (paramName)
                    {
                        case "proxyUrlIE":
                            ParameterTools.setSystemProxyUrl(paramValue);
                            break;

                        case "hideControlPanel":
                            ParameterTools.hideControlPanel(paramValue);
                            break;

                        case "monitorSleep":
                            ParameterTools.setMonitorSleep(paramValue);
                            break;

                        case "systemSleep":
                            ParameterTools.setSystemSleep(paramValue);
                            break;

                        case "proxyScriptIE":
                            ParameterTools.setSystemProxyScript(paramValue);
                            break;

                        case "proxyBypassIE":
                            ParameterTools.setSystemProxyBypass(paramValue);
                            break;

                        case "disableRegistry":
                            ParameterTools.disableRegistry(paramValue);
                            break;

                        case "changeHomePageIE":
                            ParameterTools.changeHomePageIE(paramValue);
                            break;

                        case "disallowRun":
                            ParameterTools.disallowRun(paramValue);
                            break;

                        case "bindHomeFolder":
                            ParameterTools.bindHomeFolder(paramValue);
                            break;

                        case "hiddenDrives":
                            ParameterTools.hiddenDrives(paramValue);
                            break;

                        case "shutdownTime":
                            ParameterTools.shutdownTime(paramValue);
                            break;

                        case "changeHomePageFF":
                            ParameterTools.setFirefoxHomepage(paramValue);
                            break;

                        case "proxyScriptFF":
                            ParameterTools.setFirefoxProxyScript(paramValue);
                            break;

                        case "proxyBypassFF":
                            ParameterTools.setFirefoxProxyBypass(paramValue);
                            break;

                        case "proxyUrlFF":
                            ParameterTools.setFirefoxProxyUrl(paramValue);
                            break;

                        case "proxyTypeFF":
                            ParameterTools.setFirefoxProxyType(paramValue);
                            break;

                            
                        default: Log.add("The parameter " + paramName + " is not recognized", Log.level.debug); break;
                    }

                }

                // restart explorer
                //Host.restartExplorer();

                // stop firefox
                //Host.stopFirefox();
            }
            catch (Exception ex)
            {
                Log.add("setAllParameters Error : " + ex.Message  + " --> " + ex.StackTrace, Log.level.error);
            }

        }




        /// <summary>
        /// Set monitor sleep delay
        /// </summary>
        /// <param name="value">Delay in minutes</param>
        static public void setMonitorSleep(string value)
        {
            try
            {

                var proc1 = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo
                    {
                        FileName = "powercfg.exe",
                        Arguments = "-change -monitor-timeout-ac " + value,
                        UseShellExecute = false,
                        CreateNoWindow = true
                    }
                };

                proc1.Start();

                var proc2 = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo
                    {
                        FileName = "powercfg.exe",
                        Arguments = "-change -monitor-timeout-dc " + value,
                        UseShellExecute = false,
                        CreateNoWindow = true
                    }
                };

                proc2.Start();

                Log.add("-- Set option monitorSleep to " + value, Log.level.trivial);


            }
            catch (Exception ex)
            {
                Log.add("Erreur setting option monitorSleep to " + value + " (" + ex.Message  + " --> " + ex.StackTrace + ")", Log.level.trivial);
            }
        }





        /// <summary>
        /// Set system sleep delay
        /// </summary>
        /// <param name="value">Delay in minutes</param>
        static public void setSystemSleep(string value)
        {

            try
            {

                var proc1 = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo
                    {
                        FileName = "powercfg.exe",
                        Arguments = "-change -standby-timeout-ac " + value,
                        UseShellExecute = false,
                        CreateNoWindow = true
                    }
                };

                proc1.Start();

                var proc2 = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo
                    {
                        FileName = "powercfg.exe",
                        Arguments = "-change -standby-timeout-dc " + value,
                        UseShellExecute = false,
                        CreateNoWindow = true
                    }
                };

                proc2.Start();

                Log.add("-- Set option systemSleep to " + value, Log.level.trivial);


            }
            catch (Exception ex)
            {
                Log.add("Erreur setting option systemSleep to " + value + " (" + ex.Message  + " --> " + ex.StackTrace + ")", Log.level.trivial);
            }
        }



        /// <summary>
        /// Set the proxy of the system (ie) 
        /// </summary>
        /// <param name="value"></param>
        static public void setSystemProxyUrl(string value)
        {

            string msid = Config.getUserMSID(Config.getLoguedUsername());

            // enable the system proxy
            if (value.Trim() != "")
            {
                RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyServer", value);
                RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyEnable", 1);

                Log.add("-- Set option proxyUrlIE to " + value, Log.level.trivial);
            }

            // disable the system proxy
            else
            {
                RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyServer", "");
                RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyEnable", 0);

                Log.add("-- Set option proxyUrlIE to DISABLED", Log.level.trivial);
            }

        }



        /// <summary>
        /// Set the system proxy script
        /// </summary>
        /// <param name="value"></param>
        static public void setSystemProxyScript(string value)
        {
            string msid = Config.getUserMSID(Config.getLoguedUsername());

            RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "AutoConfigURL", value);

            if (value != "")
            {
                Log.add("-- Set option proxyScriptIE to " + value, Log.level.trivial);
            }
            else
            {
                Log.add("-- Set option proxyScriptIE to NOTHING", Log.level.trivial);
            }

            
        }



        /// <summary>
        /// Proxy bypass URLs
        /// </summary>
        /// <param name="value"></param>
        static public void setSystemProxyBypass(string value)
        {

            try
            {
                if (value.Trim() != "")
                {
                    // change the separator
                    value = value.Replace(",", ";");

                    string msid = Config.getUserMSID(Config.getLoguedUsername());

                    if (RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyOverride", value))
                    {
                        Log.add("-- Set option proxyBypassIE to " + value, Log.level.trivial);
                    }

                }
            }
            catch (Exception ex)
            {
                Log.add("Error in setSystemProxyBypass Function : " + ex.Message, Log.level.error);
            }
        }



        /// <summary>
        /// Toggle the control panel
        /// </summary>
        /// <param name="value"></param>
        static public void hideControlPanel(string value)
        {

            try
            {
                string msid = Config.getUserMSID(Config.getLoguedUsername());

                // hide the CP
                if (value.Trim() == "1")
                {
                    if (RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\SOFTWARE\Microsoft\Windows\CurrentVersion\policies\Explorer", "NoControlPanel", 1))
                    {
                        Log.add("-- Set option hideControlPanel to " + value + "(Control Panel is HIDDEN)", Log.level.trivial);
                    }
                }

                // show the CP
                else
                {

                    if (RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\SOFTWARE\Microsoft\Windows\CurrentVersion\policies\Explorer", "NoControlPanel", 0))
                    {
                        Log.add("-- Set option hideControlPanel to " + value + "(Control Panel is SHOWN)", Log.level.trivial);
                    }

                    
                }
            }
            catch (Exception ex)
            {
                Log.add("Error in hideControlPanel Function : " + ex.Message, Log.level.error);
            }




        }



        /// <summary>
        /// Toggle access to the registry
        /// </summary>
        /// <param name="value"></param>
        static public void disableRegistry(string value)
        {

            try
            {
                string msid = Config.getUserMSID(Config.getLoguedUsername());

                // disable the registry
                if (value.Trim() == "1")
                {
                    if (RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Policies\System", "DisableRegistryTools", 1))
                    {
                        Log.add("-- Set option disableRegistry to " + value + "(Registry access is DISABLED)", Log.level.trivial);
                    }
                }

                // enable the registry
                else
                {
                    if (RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Policies\System", "DisableRegistryTools", 0))
                    {
                        Log.add("-- Set option disableRegistry to " + value + "(Registry access is ENABLED)", Log.level.trivial);
                    }


                }
            }
            catch (Exception ex)
            {
                Log.add("Error in disableRegistry Function : " + ex.Message, Log.level.error);
            }




        }




        /// <summary>
        /// Change IE browser homepage
        /// </summary>
        /// <param name="value"></param>
        static public void changeHomePageIE(string value)
        {

            try
            {
                if (value.Trim() != "")
                {

                    string msid = Config.getUserMSID(Config.getLoguedUsername());

                    if (RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Internet Explorer\Main", "Start Page", value))
                    {
                        Log.add("-- Set option changeHomePageIE to " + value, Log.level.trivial);
                    }

                }
            }
            catch (Exception ex)
            {
                Log.add("Error in changeHomePageIE Function : " + ex.Message, Log.level.error);
            }
        }


        

        /// <summary>
        /// Disable softwares
        /// </summary>
        /// <param name="value"></param>
        static public void disallowRun(string value)
        {

            try
            {
                string msid = Config.getUserMSID(Config.getLoguedUsername());
                
                if (value.Trim() != "")
                {

                    // create disallowRun key
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer", "DisallowRun", 1);

                    // make DisallowRun subkey if it not exists or clear it
                    RegTools.DeleteSubKeyTree(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun");
                    RegTools.CreateRegistrySubKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer", "DisallowRun");

                    int keyName = 1;


                    foreach (string app in value.Split(','))
                    {
                        if (app.Trim() != "")
                        {
                            RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun", keyName.ToString(), app.Trim());
                            Log.add("-- Disallow run for : " + app.Trim(), Log.level.trivial);
                        }

                        keyName++;
                    }

                }
            }
            catch (Exception ex)
            {
                Log.add("Error in disallowRun Function : " + ex.Message, Log.level.error);
            }
        }




        /// <summary>
        /// Hide some drives
        /// </summary>
        /// <param name="value">Drives to hide (ex : C,D,E)</param>
        static public void hiddenDrives(string value)
        {

            try
            {

                // decimal value of the string (sorta)
                int decValue = getHiddenDrivesValue(value.Trim());
                string msid = Config.getUserMSID(Config.getLoguedUsername());

                // hide no drive
                if (decValue == 0)
                {

                    if (RegTools.ExistRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer", "NoDrives"))
                    {
                        RegTools.DeleteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer", "NoDrives");
                        Log.add("-- Set option hiddenDrives to ALL VISIBLE", Log.level.trivial);
                    }

                }

                // hide some drives
                else
                {
                    if (RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer", "NoDrives", decValue))
                    {
                        Log.add("-- Set option hiddenDrives to HIDDEN (" + value + ")", Log.level.trivial);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.add("Error in hiddenDrives Function : " + ex.Message, Log.level.error);
            }
        }


        /// <summary>
        /// get the value for a hidden drives chain
        /// </summary>
        /// <param name="letters">List of letters, (separator is ',')</param>
        /// <returns></returns>
        static private int getHiddenDrivesValue(string letters)
        {

            int sum = 0;

            foreach (string letter in letters.Split(','))
            {

                switch (letter.Trim()) {
                    case "A": sum += 1; break;
                    case "B": sum += 2; break;
                    case "C": sum += 4; break;
                    case "D": sum += 8; break;
                    case "E": sum += 16; break;
                    case "F": sum += 32; break;
                    case "G": sum += 64; break;
                    case "H": sum += 128; break;
                    case "I": sum += 256; break;
                    case "J": sum += 512; break;
                    case "K": sum += 1024; break;
                    case "L": sum += 2048; break;
                    case "M": sum += 4096; break;
                    case "N": sum += 8192; break;
                    case "O": sum += 16384; break;
                    case "P": sum += 32768; break;
                    case "Q": sum += 65536; break;
                    case "R": sum += 131072; break;
                    case "S": sum += 262144; break;
                    case "T": sum += 524288; break;
                    case "U": sum += 1048576; break;
                    case "V": sum += 2097152; break;
                    case "W": sum += 4194304; break;
                    case "X": sum += 8388608; break;
                    case "Y": sum += 16777216; break;
                    case "Z": sum += 33554432; break;
                    default: sum += 0; break;
                }
            }

            return sum;
        }




        /// <summary>
        /// Bind a drive letter with pics, vids, musics and docs folders
        /// </summary>
        /// <param name="value">Share letter to bind</param>
        static public void bindHomeFolder(string letter)
        {

            try
            {

                string msid = Config.getUserMSID(Config.getLoguedUsername());


                // bind
                if (letter.Trim() != "")
                {
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "{374DE290-123F-4565-9164-39C4925E467B}", letter.Trim() + @":\Downloads");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "Cookies", letter.Trim() + @":\Cookies");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "Favorites", letter.Trim() + @":\Favorites");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "History", letter.Trim() + @":\History");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "My Music", letter.Trim() + @":\Music");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "My Pictures", letter.Trim() + @":\Pictures");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "My Video", letter.Trim() + @":\Videos");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "Personal", letter.Trim() + @":\");
                }

                // no bind
                else
                {
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "{374DE290-123F-4565-9164-39C4925E467B}", @"%USERPROFILE%\Downloads");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "Cookies", @" %USERPROFILE %\AppData\Local\Microsoft\Windows\INetCookies");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "Favorites", @"%USERPROFILE%\Favorites");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "History", @"%USERPROFILE%\AppData\Local\Microsoft\Windows\History");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "My Music", @"%USERPROFILE%\Music");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "My Pictures", @"%USERPROFILE%\Pictures");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "My Video", @"%USERPROFILE%\Videos");
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders", "Personal", @"%USERPROFILE%\Documents");
                }


                Log.add("-- Set option bindHomeFolder to " + letter, Log.level.trivial);

            }
            catch (Exception ex)
            {
                Log.add("Error in bindHomeFolder Function : " + ex.Message, Log.level.error);
            }
        }





        /// <summary>
        /// Schedule a shutdown time
        /// </summary>
        /// <param name="value">schedules hour:minutes</param>
        static public void shutdownTime (string value)
        {

            try {

                if (value.Trim() != "")
                {

                    var proc = new System.Diagnostics.Process
                    {
                        StartInfo = new System.Diagnostics.ProcessStartInfo
                        {
                            FileName = "schtasks.exe",
                            Arguments = "/Create /tn \"PARX Daily Shutdown\" /tr \"shutdown.exe -s\" /sc daily /st " + value.Trim(),
                            UseShellExecute = false,
                            CreateNoWindow = true
                        }
                    };

                    proc.Start();


                    Log.add("-- Set option shutdownTime to " + value, Log.level.trivial);
                }


                else
                {

                    var proc = new System.Diagnostics.Process
                    {
                        StartInfo = new System.Diagnostics.ProcessStartInfo
                        {
                            FileName = "schtasks.exe",
                            Arguments = "/delete /tn \"PARX Daily Shutdown\" /f",
                            UseShellExecute = false,
                            CreateNoWindow = true
                        }
                    };

                    proc.Start();
                    Log.add("-- Set option shutdownTime to NO SCHEDULE", Log.level.trivial);
                }

                

            }
            catch (Exception ex)
            {
                Log.add("Error in shutdownTime Function : " + ex.Message  + " --> " + ex.StackTrace, Log.level.error);
            }
}

       
        
        
        
        // init firefox files
        static private void initFirefoxFiles ()
        {
            try
            {
                Log.add("Firefox registry initialization", Log.level.trivial);

                string installFolder = RegTools.ReadRegistryKey(RegTools.rootKey.LocalMachine, @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\firefox.exe", "path");
                string prefFile = installFolder + @"\defaults\pref\autoconfig.js";
                string cfgFile = installFolder + @"\mozilla.cfg";

                // delete mozilla.cfg and local-settings.js
                if (System.IO.File.Exists(prefFile)) { System.IO.File.Delete(prefFile); }
                if (System.IO.File.Exists(cfgFile)) { System.IO.File.Delete(cfgFile); }

                // user id
                string msid = Config.getUserMSID(Config.getLoguedUsername());

                // create the registry keys
                RegTools.CreateRegistrySubKey(RegTools.rootKey.Users, msid + @"\Software", "Policies");
                RegTools.CreateRegistrySubKey(RegTools.rootKey.Users, msid + @"\Software\Policies", "Mozilla");
                RegTools.CreateRegistrySubKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla", "Firefox");
                Log.add("Create the Policies/Mozilla/Firefox registry keys", Log.level.debug);


                // delete existing key PROXY and create it again (faster to clear)
                RegTools.DeleteSubKeyTree(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\firefox\Proxy");
                RegTools.CreateRegistrySubKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\firefox", "Proxy");
                Log.add("Delete existing key PROXY and create it again", Log.level.debug);

                // Write the "locked" value
                RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\firefox\Proxy", "Locked", 1);

            }
            catch (Exception ex)
            {
                Log.add("Error in initFirefoxFiles (registry init) :" + ex.Message + " --> " + ex.StackTrace, Log.level.srv_error);
            }
            
        }





        /// <summary>
        /// Test if Firefox is installed
        /// </summary>
        /// <returns></returns>
        static private bool firefoxIsInstalled ()
        {
            
            string installFolder = RegTools.ReadRegistryKey(RegTools.rootKey.LocalMachine, @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\firefox.exe", "Path");

            if (installFolder == "")
            {
                return false;
            }
            else
            {
                return true;
            }

        }



        /// <summary>
        /// Set the Firefox Proxy Type
        /// </summary>
        /// <param name="proxyType">proxy type : 1=manual, 2=proxy autoconfig, 4=autodetect, 5=system proxy, 0=none</param>
        static public void setFirefoxProxyType(string proxyType)
        {
            // user id
            string msid = Config.getUserMSID(Config.getLoguedUsername());

            switch (proxyType)
            {
                // no proxy
                case "0":
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\Firefox\Proxy", "Mode", "none");
                    Log.add("-- Set Firefox proxy type to " + proxyType, Log.level.trivial);
                    break;
                
                // manual
                case "1":
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\Firefox\Proxy", "Mode", "manual");
                    Log.add("-- Set Firefox proxy type to " + proxyType, Log.level.trivial);
                    break;

                // auto config with a URL/script
                case "2":
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\Firefox\Proxy", "Mode", "autoConfig");
                    Log.add("-- Set Firefox proxy type to " + proxyType, Log.level.trivial);
                    break;

                // auto detect
                case "4":
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\Firefox\Proxy", "Mode", "autoDetect");
                    Log.add("-- Set Firefox proxy type to " + proxyType, Log.level.trivial);
                    break;
                
                // system
                case "5":
                    RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\Firefox\Proxy", "Mode", "system");
                    Log.add("-- Set Firefox proxy type to " + proxyType, Log.level.trivial);
                    break;

                default:
                    break;
            }

        }


        /// <summary>
        /// Set Firefox proxy URL
        /// </summary>
        /// <param name="proxyUrl">proxy address (ip:port)</param>
        static public void setFirefoxProxyUrl(string proxyUrl)
        {
            string msid = Config.getUserMSID(Config.getLoguedUsername());

            RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\Firefox\Proxy", "UseHTTPProxyForAllProtocols", 1);
            RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\Firefox\Proxy", "HTTPProxy", proxyUrl);
            Log.add("-- Set Firefox proxy URL to : " + proxyUrl, Log.level.trivial);

        }






        /// <summary>
        /// Set the Firefox Proxy Type
        /// </summary>
        /// <param name="proxyType">proxy type : 1=manual, 2=proxy autoconfig, 4=autodetect, 5=system proxy, 0=none</param>
        static public void setFirefoxProxyBypass(string value)
        {
            string msid = Config.getUserMSID(Config.getLoguedUsername());
            RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\firefox\Proxy", "Passthrough", value);
            Log.add("-- Set Firefox proxy BYPASS to : " + value, Log.level.trivial);
        }





        /// <summary>
        /// Set the Firefox Proxy Script
        /// </summary>
        /// <param name="proxyType">Path to the proxy script</param>
        static public void setFirefoxProxyScript(string value)
        {
            string msid = Config.getUserMSID(Config.getLoguedUsername());
            RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\firefox\Proxy", "AutoConfigURL", value);
            Log.add("-- Set Firefox proxy AUTOCONFIG URL to : " + value, Log.level.trivial);
        }






        /// <summary>
        /// Set the Firefox Proxy Type
        /// </summary>
        /// <param name="proxyType">homepages (separator is "|")</param>
        static public void setFirefoxHomepage(string value)
        {

            string msid = Config.getUserMSID(Config.getLoguedUsername());
            RegTools.CreateRegistrySubKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\firefox", "Homepage");
            

            if (value.Trim() != "")
            {
                string urls = value.Replace(';', '|');

                RegTools.WriteRegistryKey(RegTools.rootKey.Users, msid + @"\Software\Policies\Mozilla\firefox\Homepage", "URL", value);
                Log.add("-- Set Firefox HOMEPAGE URL to : " + value, Log.level.trivial);
            }
            else
            {
                Log.add("-- Set Firefox HOMEPAGE URL to : BLANK " + value, Log.level.trivial);
            }
            

        }






        /*
        		
				

"hideRunMenu"		


-- "proxyScriptFF"
-- "proxyBypassFF"
-- "changeHomePageFF"
-- "proxyUrlFF"
-- "proxyTypeFF"
-- "bindHomeFolder"	
!-- "shutdownTime"
!-- "hiddenDrives"
!-- "disallowRun"
-- "hideControlPanel"
-- "disableRegistry"
!-- "proxyUrlIE"
-- "proxyBypassIE"
-- "proxyScriptIE"
!-- "monitorSleep"
!-- "systemSleep"
!-- "changeHomePageIE"

    */





    }

}
