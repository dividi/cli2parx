﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

using Newtonsoft.Json;

using System.IO;
using System.IO.Pipes;


// https://www.codeproject.com/Tips/492231/Csharp-Async-Named-Pipes
// http://stackoverflow.com/questions/13806153/example-of-named-pipes

namespace ParxCli
{


    class Pipe
    {

        // Send an object throught the pipe to the service
        static public void pipeClient(Object task) {

            // Serialize the object
            var json = JsonConvert.SerializeObject(task);

            Console.WriteLine("task to run : " + json);
            Log.add("Task to Run : " + json, Log.level.pipe);

            //Client
            NamedPipeClientStream pipe = null;

            try
            {
                pipe = new NamedPipeClientStream(".", "ParxPipe", PipeDirection.Out);

                pipe.Connect();

                Log.add("Connecting to the pipe", Log.level.trivial);

                // Write in a pipe
                StreamWriter writer = new StreamWriter(pipe);

            }
            catch (Exception ex)
            {
                Log.add("Error connecting to the pipe : " + ex.Message, Log.level.error);
            }
            finally
            {
                pipe.Flush();
                pipe.Close();
            }

        }


        /// <summary>
        /// Send a message througth the pipe
        /// </summary>
        /// <param name="SendStr">the message</param>
        /// <param name="PipeName">the name of the pipe (default to "ParxPipe")</param>
        /// <param name="TimeOut">Connection timeout (default 2000)</param>
        public void Send(string SendStr, string PipeName= "ParxPipe", int TimeOut = 2000)
        {
            try
            {
                NamedPipeClientStream pipeStream = new NamedPipeClientStream(".", PipeName, PipeDirection.Out, PipeOptions.Asynchronous, System.Security.Principal.TokenImpersonationLevel.Impersonation, HandleInheritability.None);

                // The connect function will indefinitely wait for the pipe to become available
                // If that is not acceptable specify a maximum waiting time (in ms)
                pipeStream.Connect(TimeOut);

                Log.add("Sending : " + SendStr, Log.level.debug);

                byte[] _buffer = Encoding.UTF8.GetBytes(SendStr);
                pipeStream.BeginWrite(_buffer, 0, _buffer.Length, AsyncSend, pipeStream);

            }
            catch (TimeoutException ex)
            {
                Log.add("Pipe Send Error : " + ex.Message, Log.level.error);
            }
        }






        /// <summary>
        /// Asynchronous stream send
        /// </summary>
        /// <param name="iar"></param>
        private void AsyncSend(IAsyncResult iar)
        {
            try
            {
                // Get the pipe
                NamedPipeClientStream pipeStream = (NamedPipeClientStream)iar.AsyncState;

                // End the write
                pipeStream.EndWrite(iar);
                pipeStream.Flush();
                pipeStream.Close();
                pipeStream.Dispose();
            }
            catch (Exception ex)
            {
                Log.add("Async Pipe Send : " + ex.Message, Log.level.error);
            }
        }






        /// <summary>
        /// Wrapper method to send data in the pipe
        /// </summary>
        /// <param name="taskID"></param>
        /// <param name="taskName"></param>
        /// <param name="taskParameters">An object with </param>
        static public void sendJsonToPipe(string taskID, string taskName, Object taskParameters = null)
        {
            try
            {

                // new pipe            
                Pipe _pipeClient = new Pipe();

                // make a task object
                Object obj = new
                {
                    name = taskName,
                    id = taskID,
                    parameters = taskParameters
                };

                _pipeClient.Send(JsonConvert.SerializeObject(obj));

            }
            catch (Exception ex)
            {
                Log.add("Error in sendJsonToPipe function : " + ex.Message, Log.level.error);
            }


        }



    }
}









/*
static public void StartServer()
{
    Task.Factory.StartNew(() =>
    {
        var server = new NamedPipeServerStream("parxPipe");
        server.WaitForConnection();
        StreamReader reader = new StreamReader(server);
        StreamWriter writer = new StreamWriter(server);
        while (true)
        {
            var line = reader.ReadLine();
            writer.WriteLine(String.Join("", line.Reverse()));
            writer.Flush();
        }
    });
}
*/
