﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParxCli.Classes
{

    
    /// <summary>
    /// Class to wrap the pivot part of the share
    /// </summary>
    class SharePivot
    {
        public string letter;

        public SharePivot (string shareLetter)
        {
            this.letter = shareLetter;
        }
    }



    /// <summary>
    /// The main share class
    /// </summary>
    class Share
    {

        public int _id;
        public string _name;
        public string _target;
        public string _letter;
                

        /// <summary>
        /// Constructor : create a new share object
        /// </summary>
        public Share(string name, string target, SharePivot pivot)
        {
            this._name = name;
            this._target = target;
            this._letter = pivot.letter;
        }



        /// <summary>
        /// Mount a share using "net use" command
        /// </summary>
        public void mount ()
        {
            try
            {
                
                var proc = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo
                    {
                        FileName = "net.exe",
                        Arguments = "use " + this._letter + ": \"" + this._target + "\"",
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                proc.Start();

                // use this block to display pin output
                /*while (!proc.StandardOutput.EndOfStream)
                {
                    string line = proc.StandardOutput.ReadLine();
                    Log.add("-- try to pin : " + proc.StartInfo.Arguments, Log.level.debug);
                    Log.add("-- XXXXXXXXx : " + line, Log.level.debug);
                }*/

                Log.add("-- Mount share : " + this._name + " (" + this._target + ") on " + this._letter, Log.level.trivial);

            }
            catch (Exception ex)
            {
                Log.add("Share mount " + this._letter + " (" + ex.Message + ")", Log.level.debug);
            }
            
        }




    }
}
