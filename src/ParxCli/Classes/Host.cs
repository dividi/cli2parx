﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using IWshRuntimeLibrary;
using System.IO;


namespace ParxCli.Classes
{
    class Host
    {

        
        /// <summary>
        /// Stop the Explorer process and let the system restart it
        /// </summary>
        static public void restartExplorer ()
        {
            foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName("explorer"))
            {
                try
                {
                    Log.add("Restart explorer process", Log.level.trivial);
                    p.Kill();
                    return;
                }
                catch (Exception ex)
                {
                    Log.add("Error in restartExplorer Function : " + ex.Message, Log.level.error);
                }

            }
        }



        /// <summary>
        /// start userinit
        /// </summary>
        static public void startUserinit()
        {
            //string userinit = string.Format("{0}\\{1}", Environment.GetEnvironmentVariable("SYSTEM32"), "userinit.exe");
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = @"C:\Windows\System32\userinit.exe";
            process.StartInfo.UseShellExecute = true;
            process.Start();
        }


        /// <summary>
        /// Stop the firefox process
        /// </summary>
        static public void stopFirefox()
        {
            foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName("firefox"))
            {
                try
                {
                    Log.add("Stop Firefox process", Log.level.trivial);
                    p.Kill();
                    return;
                }
                catch (Exception ex)
                {
                    Log.add("Error in stopFirefox Function : " + ex.Message, Log.level.error);
                }

            }
        }



        /// <summary>
        /// Stop the explorer (only from the service)
        /// </summary>
        static public void stopExplorer() {

            // Disable auto restart explorer
            RegTools.WriteRegistryKey(RegTools.rootKey.LocalMachine, @"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "AutoRestartShell", 0);

            foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName("explorer"))
            {
                try
                {
                    Log.add("Restart explorer process", Log.level.trivial);
                    p.Kill();
                    return;
                }
                catch (Exception ex)
                {
                    Log.add("Error in restartExplorer Function : " + ex.Message, Log.level.error);
                }

            }
        }


        /// <summary>
        /// start the explorer
        /// </summary>
        static public void startExplorer() {

            string explorer = string.Format("{0}\\{1}", Environment.GetEnvironmentVariable("WINDIR"), "explorer.exe");
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = explorer;
            process.StartInfo.UseShellExecute = true;
            process.Start();
        }

       
        /// <summary>
        /// Restart Cli2Parx
        /// </summary>
        static public void clientRestart()
        {
            System.Windows.Forms.Application.Restart();
            Log.add("Restart Client", Log.level.trivial);
        }


        /// <summary>
        /// Exit Cli2Parx
        /// </summary>
        static public void clientExit()
        {
            System.Windows.Forms.Application.Exit();
            Log.add("Exit Client, See you space cowboy... ", Log.level.trivial);
        }


        /// <summary>
        /// force Logout the user
        /// </summary>
        static public void userLogout()
        {
            var proc = new System.Diagnostics.Process
            {
                StartInfo = new System.Diagnostics.ProcessStartInfo
                {
                    FileName = "shutdown.exe",
                    Arguments = "/l /f",
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };

            proc.Start();
        }





        /// <summary>
        /// Clear the current user desktop (clearPublicDesktop can be found in shortcutTools class)
        /// </summary>
        /// <param name="deleteFiles">Delete shortcuts AND files, folders...</param>
        /// <param name="onlyParxShortcuts">Delete only Parx made shortcuts</param>
        static public void clearUserDesktop(bool deleteFiles, bool onlyParxShortcuts)
        {

            Log.add("Clear USER desktop", Log.level.info);

            // If delete all files (also delete PARX shortcuts)
            if (deleteFiles)
            {

                Log.add("Delete files", Log.level.info);

                DirectoryInfo di = new DirectoryInfo(Config.getUserDesktopPath());

                // delete all files
                foreach (FileInfo file in di.GetFiles())
                {
                    Log.add("-- Delete file : " + file.Name, Log.level.trivial);
                    file.Delete();
                }

                // delete all folders
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    Log.add("-- Delete folder : " + dir.FullName, Log.level.trivial);
                    dir.Delete(true);
                }


            }

            // If delete only shortcuts
            else
            {
                var files = Directory.EnumerateFiles(Config.getUserDesktopPath(), "*.lnk");

                // delete only shortcuts with [PARX]
                if (onlyParxShortcuts)
                {

                    Log.add("Delete PARX shortcuts", Log.level.info);

                    WshShell shell = new WshShell();

                    foreach (var file in files)
                    {

                        string description = shell.CreateShortcut(file).Description;

                        if (description.Contains("[PARX]"))
                        {
                            Log.add("-- Delete shortcut : " + file, Log.level.trivial);
                            System.IO.File.Delete(file);
                        }

                    }

                }

                // delete all shortcuts
                else
                {
                    Log.add("Delete ALL shortcuts", Log.level.info);

                    foreach (var file in files)
                    {
                        Log.add("-- Delete file : " + file, Log.level.trivial);
                        System.IO.File.Delete(file);
                    }
                }


            }

        }




        /// <summary>
        /// clear the taskbar of all shortcuts
        /// </summary>
        static public void clearTaskbar() {

            Log.add("Clear the taskbar", Log.level.info);

            try
            {
                // delete the reg key
                RegTools.DeleteSubKeyTree(RegTools.rootKey.CurrentUser, @"Software\Microsoft\Windows\CurrentVersion\Explorer\Taskband");

                // delete the folder (don't know what is it for)
                string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar";

                string[] files = Directory.GetFiles(folder);
                foreach (string file in files)
                {
                    Log.add("-- Delete file : " + file, Log.level.trivial);
                    System.IO.File.Delete(file);
                }

            }
            catch (Exception ex)
            {
                Log.add("Error in function clearTaskBar : " + ex.Message, Log.level.error);
            }
            

        }




        /// <summary>
        /// get a shortcut list from a local path
        /// </summary>
        /// <param name="path">the path where to find shortcuts</param>
        /// <param name="shortcutType">0=desktop, 1=taskbar</param>
        /// <returns></returns>
        static public List<Shortcut> getShortcuts(string path, int shortcutType)
        {

            List<Shortcut> shortcutsList = new List<Shortcut>();

            // using Wshell
            WshShell shell = new WshShell();

            // get user desktop shortcuts
            var shortcuts = Directory.EnumerateFiles(path, "*.lnk");
            

            // for each shortcuts in all desktops
            foreach (var shortcut in shortcuts)
            {

                // get shortcut data
                IWshShortcut sc = (IWshShortcut)shell.CreateShortcut(shortcut);

                // create a shortcut
                Shortcut shortcutInstance = new Shortcut(Path.GetFileNameWithoutExtension(sc.FullName), sc.TargetPath, sc.Arguments, sc.WorkingDirectory, sc.Description, shortcutType);

                // add it to the list
                shortcutsList.Add(shortcutInstance);

            }

            return shortcutsList;

        }
        



        /// <summary>
        /// Delete all shares except U
        /// </summary>
        static public void clearShares ()
        {
            Log.add("Unmount all shares (except U)", Log.level.info);

            ManagementObjectSearcher objSearcher = new ManagementObjectSearcher(@"SELECT Name, VolumeName FROM Win32_LogicalDisk WHERE DriveType=4");
            ManagementObjectCollection objCollection = objSearcher.Get();

            foreach (var obj in objCollection)
            {

                if (obj["Name"].ToString() != "U:")
                {
                    try
                    {
                        
                        var proc = new System.Diagnostics.Process
                        {
                            StartInfo = new System.Diagnostics.ProcessStartInfo
                            {
                                FileName = AppDomain.CurrentDomain.BaseDirectory + "net.exe",
                                Arguments = "use " + obj["Name"] + " /del /y",
                                UseShellExecute = false,
                                CreateNoWindow = true
                            }
                        };

                        proc.Start();

                        Log.add("-- Unmount share " + obj["Name"], Log.level.trivial);

                    }
                    catch (Exception ex)
                    {
                        Log.add("Unmount share " + obj["Name"] + " error : " + ex.Message, Log.level.error);
                    }
                }
                
            }

        }



        /// <summary>
        /// Delete all local and network printers
        /// </summary>
        static public void clearPrinters ()
        {
            Log.add("Unmount all printers", Log.level.info);

            ManagementObjectSearcher objSearcher = new ManagementObjectSearcher(@"SELECT * FROM Win32_Printer");
            ManagementObjectCollection objCollection = objSearcher.Get();

            foreach (var obj in objCollection)
            {
                try
                {
                    WshNetwork objNetwork = new WshNetwork();
                    WshNetwork oNetwork = new WshNetwork();

                    oNetwork.RemoveNetworkDrive((string)obj["Name"], true, true);

                    Log.add("-- Unmount printer " + obj["Name"], Log.level.trivial);

                    /*
                    // Another way to do it  
                    ConnectionOptions options = new ConnectionOptions();
                    options.EnablePrivileges = true;
                    ManagementScope scope = new ManagementScope(ManagementPath.DefaultPath, options);
                    scope.Connect();
                    ManagementClass win32Printer = new ManagementClass("Win32_Printer");
                    ManagementObjectCollection printers = win32Printer.GetInstances();
                    foreach (ManagementObject printer in printers)
                    {
                        printer.Delete();
                    }*/


                }
                catch (Exception ex)
                {
                    Log.add("Printer " + obj["Name"] + " can't be removed (" + ex.Message + ")", Log.level.warning);
                }
                
                
            }

        }



        /// <summary>
        /// Test if service exists and is running
        /// </summary>
        /// <returns></returns>
        static public bool serviceIsRunning ()
        {
            try
            {
                System.ServiceProcess.ServiceController sc = new System.ServiceProcess.ServiceController("Cli2Parx");

                if (sc.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }

    }
}
