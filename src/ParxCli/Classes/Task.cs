﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParxCli.Classes
{

    //{"code":0,"message":"[{\"id\":8,\"name\":\"notepad\",\"exe\":\"c:\\\\windows\\\\notepad.exe\",\"args\":\"\",\"asAdmin\":0,\"description\":\"dsfgfg\",\"pivot\":{\"host_id\":11,\"task_id\":8,\"id\":1,\"recurtion\":0,\"schedule\":\"\"}}]"}


    public class TaskPivot
    {
        public string host_id;
        public string task_id;
        public string id;
        public string recurtion;
        public string schedule;

        public TaskPivot(string host_id, string task_id, string id, string recurtion, string schedule)
        {
            // this.Default = defaultPrinter == "1" ? true : false;
            this.host_id = host_id;
            this.task_id = task_id;
            this.id = id;
            this.recurtion = recurtion;
            this.schedule = schedule;
        }
    }


    

   public class Task
    {

        public string id;
        public string name;
        public string exe;
        public string args;
        public bool asAdmin;
        public string description;

        //public TaskPivot pivot;

        public string task_id;
        public string recurtion;
        public string schedule;

        public Task(string id, string name, string exe, string args, string asAdmin, string description, TaskPivot pivot )
        {
            this.id = id;
            this.name = name;
            this.exe = exe;
            this.args = args;
            this.asAdmin = asAdmin == "1" ? true : false;
            this.description = description;
            this.task_id = pivot.id;
            this.recurtion = pivot.recurtion;
            this.schedule = pivot.schedule;

        }



        public void runUserTask (bool deleteTask=true)
        {

            Log.add("User task to run : " + this.name, Log.level.info);

            var proc = new System.Diagnostics.Process
            {
                StartInfo = new System.Diagnostics.ProcessStartInfo
                {
                    FileName = this.exe,
                    Arguments = this.args,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            proc.Start();

            if (deleteTask)
            {

                this.deleteTask();
            }

        }



        public void deleteTask ()
        {
            Log.add("Delete task " + this.name + " (" + this.task_id + ")", Log.level.info);

            // rest client
            Rest restClient = new Rest(Config.serverURI);

            restClient.deleteTask(this.task_id);

            

        }


     
    }




    /// <summary>
    /// 
    /// </summary>
    public class TaskTools
    {



       /// <summary>
       /// Run a task throught the service (can be scheduled)
       /// </summary>
       /// <param name="name"></param>
       /// <param name="exe"></param>
       /// <param name="args"></param>
       /// <param name="schedule"></param>
       static public void runAdminTask(string name, string exe, string args, string schedule)
        {

            Log.add("Admin task to run : " + name, Log.level.info);

            try
            {
                var proc = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo
                    {
                        FileName = exe,
                        Arguments = args,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                proc.Start();

            }
            catch (Exception ex)
            {
                Log.add("Error in runAdminTask : " + ex.Message, Log.level.error);
            }
        }



        /// <summary>
        /// add a scheduled task to the windows crontab (ohoho)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="exe"></param>
        /// <param name="args"></param>
        /// <param name="schedule"></param>
        static public void addSheduledAdminTask(string id, string name, string exe, string args, string schedule)
        {

            Log.add("Sheduled Admin task to run : " + name, Log.level.info);


            string taskname = "parx-run" + id;
            string[] sch = schedule.Split(';');
            string schType = sch[0];
            string schAt = sch[1];
            string schDays = sch[2];

            string schExe = exe + " " + args;

            string type = "";
            string at = "";
            string days = "";

            // type
            type = " /sc " + schType;

            // at
            if (schType == "Weekly" ||schType == "Once")
            {
                at = " /st " + schAt;
            }

            // days
            if (schType == "Weekly")
            {
                days = " /d" + schDays;
            }

            string schArgs = "/create /tn " + taskname + " /ru System /tr \"" + schExe +"\" " + type + at + days;

            Log.add("Scheduled task args : " + schArgs, Log.level.debug);
            
            try
            {
                var proc = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo
                    {
                        FileName = "schtasks.exe",
                        Arguments = schArgs,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                proc.Start();

            }
            catch (Exception ex)
            {
                Log.add("Error in runSheduledAdminTask : " + ex.Message, Log.level.error);
            }
        }



        /// <summary>
        /// delete a scheduled task
        /// </summary>
        /// <param name="taskID"></param>
        static public void deleteScheduledAdminTask (string taskID)
        {

            Log.add("Delete Scheduled admin task : " + taskID, Log.level.info);

            if (taskID != "")
            {
                string taskname = "parx-run" + taskID;

                string schArgs = "/delete /tn " + taskname + " /f";

                try
                {
                    var proc = new System.Diagnostics.Process
                    {
                        StartInfo = new System.Diagnostics.ProcessStartInfo
                        {
                            FileName = "schtasks.exe",
                            Arguments = schArgs,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                            CreateNoWindow = true
                        }
                    };

                    proc.Start();

                }
                catch (Exception ex)
                {
                    Log.add("Error in deleteScheduledAdminTask : " + ex.Message, Log.level.error);
                }

            }
            else
            {
                Log.add("No task ID.", Log.level.warning);
            }
            
           
        }




        static public void runTask (Classes.Task task)
        {

            // special case for update tasks
            if (task.name == "update")
            {
                Pipe.sendJsonToPipe(task.task_id, "updateClient", "");
            }

            // other tasks
            else
            {

                // admin task
                if (task.asAdmin)
                {

                    var parameters = new { name = task.name, exe = task.exe, args = task.args, schedule = task.schedule };


                    // non scheduled task
                    if (task.schedule == "")
                    {


                        // delete scheduled admin task
                        if (task.name == "delete-schTask")
                        {
                            Pipe.sendJsonToPipe(task.task_id, "deleteSheduledAdminTask", parameters);
                        }

                        // regular admin task
                        else
                        {
                            Pipe.sendJsonToPipe(task.task_id, "runAdminTask", parameters);
                        }

                    }


                    // scheduled admin task
                    else
                    {
                        Pipe.sendJsonToPipe(task.task_id, "addSheduledAdminTask", parameters);
                    }

                }

                // regular user task
                else
                {
                    task.runUserTask();
                }
            }

        }

    }


}
