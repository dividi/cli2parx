﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Win32;

namespace ParxCli.Classes
{


    public class RegTools
    {


        public enum rootKey
        {
            LocalMachine, Users, CurrentUser
        }


        /// <summary>
        /// get an enum and change it to a beautiful RegistryKey
        /// </summary>
        /// <param name="root">the rootkey</param>
        /// <returns>a base RegistryKey</returns>
        static private RegistryKey getBaseRegistryKey(rootKey root)
        {

            RegistryKey baseRegistryKey;

            if (root == rootKey.LocalMachine)
            {
                baseRegistryKey = Registry.LocalMachine;
            }
            else if (root == rootKey.Users)
            {
                baseRegistryKey = Registry.Users;
            }
            else if (root == rootKey.CurrentUser)
            {
                baseRegistryKey = Registry.CurrentUser;
            }
            else
            {
                baseRegistryKey = null;
            }


            return baseRegistryKey;
        }




        /// <summary>
        /// read the value of a reg key
        /// </summary>
        /// <param name="root">root key (hkcu, hku, hklm)</param>
        /// <param name="subKey">path to the key</param>
        /// <param name="KeyName">the key</param>
        /// <returns></returns>
        static public string ReadRegistryKey(rootKey root, string subKey, string KeyName)
        {

            RegistryKey baseRegistryKey = getBaseRegistryKey(root);

            // Open a subKey as read-only
            RegistryKey sk1 = baseRegistryKey.OpenSubKey(subKey);

            // If the RegistrySubKey doesn't exist -> (null)
            if (sk1 == null)
            {
                return null;
            }
            else
            {

                try
                {
                    // If the RegistryKey exists I get its value or null is returned.
                    return (string)sk1.GetValue(KeyName.ToUpper());
                }
                catch (Exception e)
                {
                    Log.add("ReadRegistryKey Error : " + e.Message, Log.level.reg_error);
                    Log.add("Subkey:" + subKey + " --- Keyname:" + KeyName, Log.level.reg_error);
                    return null;
                }
            }
        }



        /// <summary>
        /// Create/Edit a registry key
        /// </summary>
        /// <param name="root">the root registry key</param>
        /// <param name="subKey">the key path</param>
        /// <param name="KeyName">the key to create or edit</param>
        /// <param name="Value">the value of the key</param>
        /// <returns></returns>
        static public bool WriteRegistryKey(rootKey root, string subKey, string KeyName, object Value)
        {

            try
            {
                RegistryKey baseRegistryKey = getBaseRegistryKey(root);

                RegistryKey sk1 = baseRegistryKey.CreateSubKey(subKey);

                // Save the value
                sk1.SetValue(KeyName, Value);

                return true;
            }
            catch (Exception e)
            {
                Log.add("WriteRegistryKey Error : " + e.Message, Log.level.reg_error);
                Log.add("Subkey:" + subKey + " --- Keyname:" + KeyName + " --- Value:" + Value, Log.level.reg_error);
                return false;
            }
        }




        /// <summary>
        /// Create a new subkey
        /// </summary>
        /// <param name="root">the root registry key</param>
        /// <param name="subKey">the parent key path</param>
        /// <param name="SubKeyName">the subkey to create</param>
        /// <returns></returns>
        static public bool CreateRegistrySubKey(rootKey root, string subKey, string SubKeyName)
        {

            try
            {
                RegistryKey baseRegistryKey = getBaseRegistryKey(root);

                RegistryKey sk1 = baseRegistryKey.CreateSubKey(subKey);

                // Create the new Subkey
                sk1.CreateSubKey(SubKeyName);

                return true;
            }
            catch (Exception e)
            {
                Log.add("CreateRegistrySubKey Error : " + e.Message, Log.level.reg_error);
                Log.add("Subkey:" + subKey + " --- SubKeyname:" + SubKeyName, Log.level.reg_error);
                return false;
            }
        }


        /// <summary>
        /// Delete a registry key
        /// </summary>
        /// <param name="root">the root registry key</param>
        /// <param name="subKey">the key path</param>
        /// <param name="KeyName">the key to delete</param>
        /// <returns></returns>
        static public bool DeleteRegistryKey(rootKey root, string subKey, string KeyName)
        {
            /*try
            {*/
                RegistryKey baseRegistryKey = getBaseRegistryKey(root);

                RegistryKey sk1 = baseRegistryKey.CreateSubKey(subKey);

                // If the RegistrySubKey doesn't exists -> (true)
                if (sk1 == null)
                    return true;
                else
                    sk1.DeleteValue(KeyName);

                return true;
            /*}
            catch (Exception e)
            {
                Log.add("DeleteRegistryKey Error : " + e.Message, Log.level.reg_error);
                Log.add("Subkey:" + subKey + " --- Keyname:" + KeyName, Log.level.reg_error);
                return false;
            }*/
        }





        /// <summary>
        /// Test if a key exists
        /// </summary>
        /// <param name="root">the root registry key</param>
        /// <param name="subKey">the key path</param>
        /// <param name="KeyName">the key to delete</param>
        /// <returns>true if exists</returns>
        static public bool ExistRegistryKey(rootKey root, string subKey, string KeyName)
        {

            RegistryKey baseRegistryKey = getBaseRegistryKey(root);

            RegistryKey sk1 = baseRegistryKey.CreateSubKey(subKey);

            if (sk1 == null)
                return false;
            else
                return true;
        }




        /// <summary>
        /// Delete a key and all subkeys
        /// </summary>
        /// <param name="root">the root registry key</param>
        /// <param name="subKey">the key path to delete</param>
        /// <returns></returns>
        static public bool DeleteSubKeyTree(rootKey root, string subKey)
        {
            try
            {
                RegistryKey baseRegistryKey = getBaseRegistryKey(root);

                RegistryKey sk1 = baseRegistryKey.OpenSubKey(subKey);

                // If the RegistryKey exists, I delete it
                if (sk1 != null)
                    baseRegistryKey.DeleteSubKeyTree(subKey);

                return true;
            }
            catch (Exception e)
            {
                Log.add("DeleteSubKeytree Error : " + e.Message, Log.level.reg_error);
                Log.add("Subkey:" + subKey, Log.level.reg_error);
                return false;
            }
        }



        /// <summary>
        /// Count the number of subkeys of a subkey
        /// </summary>
        /// <param name="root">the root registry key</param>
        /// <param name="subKey">the key path to delete</param>
        /// <returns>the subkey elements count</returns>
        static public int CountRegistrySubKey(rootKey root, string subKey)
        {
            try
            {
                RegistryKey baseRegistryKey = getBaseRegistryKey(root);

                RegistryKey sk1 = baseRegistryKey.OpenSubKey(subKey);

                // If the RegistryKey exists...
                if (sk1 != null)
                    return sk1.SubKeyCount;
                else
                    return 0;
            }
            catch (Exception e)
            {
                return 0;
            }
        }




        /// <summary>
        /// Count the number of elements in a subkey
        /// </summary>
        /// <param name="root">the root registry key</param>
        /// <param name="subKey">the key path to delete</param>
        /// <returns>elements count</returns>
        static public int CountSubkeyValues(rootKey root, string subKey)
        {
            try
            {
                RegistryKey baseRegistryKey = getBaseRegistryKey(root);

                RegistryKey sk1 = baseRegistryKey.OpenSubKey(subKey);

                // If the RegistryKey exists...
                if (sk1 != null)
                    return sk1.ValueCount;
                else
                    return 0;
            }
            catch (Exception e)
            {
                return 0;
            }
        }





    }

}
