﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using ParxCli.Classes;



namespace ParxCli
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string hostId;

        // the list of profiles
        List<Profile> profilesList;

        // the list o' tasks
        List<Classes.Task> tasksList;

        // the profile loaded
        Profile currentProfile;

        // check if profile admin
        bool isAdmin = false;

        // CONFIG
        static Config config = new Config(Application.StartupPath + "\\config.xml");


        // INIT

        private void Form1_Load(object sender, EventArgs e)
        {
            // set the working folder
            Directory.SetCurrentDirectory(@"c:\cli2parx");
            this.Size = new Size(1, 1);

            // CLEAR LOG ON START
            if (Config.logClearOnStart) Log.clearLogFile();

            Log.add("Hello", Log.level.trivial);
            Log.add("Host : " + Config.hostname, Log.level.info);
            Log.add("User : " + Config.username, Log.level.info);
            Log.add("MSID : " + Config.MSID, Log.level.debug);
            Log.add("Task Refresh Rate: " + Config.refreshRate, Log.level.debug);


            // test if service exists/is running
            if ( Host.serviceIsRunning() == false )
            {
                Log.add("The service is not running, some functions will not work properly.", Log.level.error);
                MessageBox.Show("The service is not running, some functions will not work properly.", "Big Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



            // REST
            Rest restClient = new Rest(Config.serverURI);

            // get host ID
            hostId = restClient.getHostID(Config.hostname);

            // Host data
            var data = new { hostName = Config.hostname, hostOs = Config.hostOS, version = Config.version };

            // Auto register Host
            if (Config.autoRegisterHost == true) {
                restClient.addHost(data);
            }

            // Update the host version + OS
            restClient.updateHost(data);


            // get profiles List
            profilesList = restClient.getProfilesList(Config.username, Config.hostname);
            

            // add version number
            cLI2ParxToolStripMenuItem.Text = "CLI2Parx v" + Config.version;





            // Normal execution
            if (profilesList != null)
            {

                // add profiles in context Menu
                if (profilesList.Count > 0)
                {
                    foreach (Profile p in profilesList)
                    {

                        // add with an item and its handler

                        ToolStripItem subItem = new ToolStripMenuItem();
                        subItem.Click += new EventHandler(MenuClicked);
                        subItem.Text = p.name;
                        profilsToolStripMenuItem.DropDownItems.Add(subItem);
                        subItem.Tag = p;

                        Log.add("Profile " + p.name + " added to context menu", Log.level.info);
                    }

                    // check and put the first profile in currentProfile
                    currentProfile = profilesList[0];
                    ToolStripMenuItem firstItem = (ToolStripMenuItem)profilsToolStripMenuItem.DropDownItems[0];
                    firstItem.Checked = true;



                    // Autoloads

                    // detect if user is admin
                    foreach (string admin in Config.adminList)
                    {
                        if (Config.loguedUsername.Contains(admin))
                        {
                            isAdmin = true;
                            Log.add("All praise to thee, Admin !", Log.level.debug);
                            break;
                        }
                    }
                    


                    // autoloads (if not admin)
                    if (!isAdmin)
                    {
                        // autoload shortcuts + taskbar
                        if ( Config.autoShortcuts )
                        {
                            currentProfile.loadDesktop(Config.whichDesktop, Config.deleteDesktopFiles, Config.deleteOnlyParxShortcuts, Config.cleanUserDesktop, Config.cleanPublicDesktop);
                            System.Threading.Thread.Sleep(2000);
                        }

                        // autoload printers
                        if (Config.autoPrinters)
                        {
                            currentProfile.loadPrinters(Config.unmountPrinters);
                        }

                        // autoload shares
                        if (Config.autoShares)
                        {
                            currentProfile.loadShares(Config.unmountShares);
                        }

                        // autoload parameters
                        if (Config.autoParameters)
                        {
                            Pipe.sendJsonToPipe("0", "loadCurrentProfileParameters", currentProfile.parameters);
                        }

                        // autoload tasks
                        if (Config.autoTasks)
                        {

                            // get tasks list
                            if (hostId != null)
                            {
                                tasksList = restClient.getTasksList(hostId);

                                if (tasksList == null)
                                {
                                    Log.add("No task found.", Log.level.trivial);
                                }
                                else
                                {
                                    Log.add(tasksList.Count + " task(s) found.", Log.level.info);

                                    // run tasks
                                    foreach (Classes.Task task in tasksList)
                                    {

                                        // run the task
                                        TaskTools.runTask(task);

                                        // delete task
                                        restClient.deleteTask(task.task_id);
                                    }
                                } // end if/else

                            } // end if


                        } // end atotasks

                    }   // end if !isadmin


                }


                // No profile mode
                else
                {
                    profilsToolStripMenuItem.Text = "AUCUN PROFIL";
                    profilsToolStripMenuItem.BackColor = Color.Salmon;

                    profilsToolStripMenuItem.Enabled = false;
                    sauverToolStripMenuItem.Enabled = false;
                    chargerToolStripMenuItem.Enabled = false;

                    // kick user without a profile (except admin !)
                    if (Config.kickWithoutProfile == true && !isAdmin)
                    {
                        Log.add("You shall not pass, user without a profile !", Log.level.warning);
                        MessageBox.Show("Aucun profil associé, vous allez être déconnecté !", "Connexion sans profil interdite", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Host.userLogout();
                    }
                }




                // Timer
                if (Config.refreshRate != "0")
                {
                    Timer.Interval = Int32.Parse(Config.refreshRate) * 1000;

                    // block Timer to 30000ms 
                    if (Timer.Interval < 30000)
                        Timer.Interval = 30000;

                    Timer.Enabled = true;
                }
                else
                {
                    Timer.Enabled = false;
                }



            }


            // if null --> no connection with server
            else
            {
                Log.add("FATAL ERROR : No connection, or bad proxy address", Log.level.error);
                MessageBox.Show("Guru meditation : No connection or bad proxy address (or not in proxy exception) !", "Big FATAL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



            // run user init
            Host.startUserinit();

            //Form1.ActiveForm.WindowState = FormWindowState.Normal;
            //this.WindowState = FormWindowState.Minimized;


        }



        // ======================
        //      Menu items
        // ======================


        // Dynamic profile menu item CLICK handler
        private void MenuClicked(object sender, EventArgs e)
        {
            ToolStripMenuItem clickedItem = (ToolStripMenuItem)sender;

            // ask to load the new profile
            DialogResult res = MessageBox.Show("Voulez vous charger ce profil (raccourcis, options, imprimantes, partages) ?", "Charger le profil", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {

                // check the line (uncheck and check the clicked one)
                foreach (ToolStripMenuItem itm in clickedItem.GetCurrentParent().Items) { itm.Checked = false;   }

                clickedItem.Checked = true;

                // get the profile stored in Tag
                currentProfile = (Profile)clickedItem.Tag;

                Log.add("Load profile " + currentProfile.name, Log.level.info);


                // load shortcuts + taskbar
                currentProfile.loadDesktop(Config.whichDesktop, Config.deleteDesktopFiles, Config.deleteOnlyParxShortcuts, Config.cleanUserDesktop, Config.cleanPublicDesktop);

                System.Threading.Thread.Sleep(2000);

                // load Printers
                currentProfile.loadPrinters(Config.unmountPrinters);

                // load Shares
                currentProfile.loadShares(Config.unmountShares);

                // load Parameters
                Pipe.sendJsonToPipe("0", "loadCurrentProfileParameters", currentProfile.parameters);
            }


            }


        // Exit menu item
        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Host.clientExit();
        }


        // Exit menu item
        private void quitterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Host.clientExit();
        }



        // restart menu item
        private void rechargerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Host.clientRestart();
        }



        // save desktop shortcuts
        private void bureauToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
            DialogResult res = MessageBox.Show("Voulez vous sauvegarder le bureau ?", "Sauvegarde du bureau", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar");

                // get shortcuts list
                List<ParxCli.Classes.Shortcut> userDesktopShortcutsList = Host.getShortcuts(Config.getUserDesktopPath(), 0);
                List<ParxCli.Classes.Shortcut> publicDesktopShortcutsList = Host.getShortcuts(Config.getPublicDesktopPath(), 0);
                List<ParxCli.Classes.Shortcut> taskbandShortcutsList = Host.getShortcuts(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar", 1);

                // concat every shortcuts
                List<ParxCli.Classes.Shortcut> shortcutsToSend = new List<Classes.Shortcut>();
                shortcutsToSend.AddRange(userDesktopShortcutsList);
                shortcutsToSend.AddRange(publicDesktopShortcutsList);
                shortcutsToSend.AddRange(taskbandShortcutsList);


                // send shortcuts list
                Rest restClient = new Rest(Config.serverURI);
                restClient.sendShortcutsList(currentProfile.id, shortcutsToSend);


                // get profiles List
                profilesList = restClient.getProfilesList(Config.username, Config.hostname);


            }

        }



        // load desktop shortcuts
        private void bureauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            currentProfile.loadDesktop(Config.whichDesktop, Config.deleteDesktopFiles, Config.deleteOnlyParxShortcuts, Config.cleanUserDesktop, Config.cleanPublicDesktop);                      
        }



        // Launch the log viewer
        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"c:\cli2parx\logviewer.exe");
        }



        // load shares of the current profile
        private void partagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            currentProfile.loadShares(Config.unmountShares);
        }



        // load printers of the current profile
        private void imprimantesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            currentProfile.loadPrinters(Config.unmountPrinters);
        }



        // load options of the current profile
        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pipe.sendJsonToPipe("0", "loadCurrentProfileParameters", currentProfile.parameters);
        }


        
        // load tasks of the current host
        private void tachesToolStripMenuItem_Click(object sender, EventArgs e)
        {

            // refresh tasks list

            // rest client
            Rest restClient = new Rest(Config.serverURI);

            // get tasks list
            if (hostId != null)
            {
                tasksList = restClient.getTasksList(hostId);

                if (tasksList == null)
                {
                    Log.add("No task found.", Log.level.trivial);
                }
                else
                {
                    Log.add(tasksList.Count + " task(s) found.", Log.level.info);

                    // run tasks
                    foreach (Classes.Task task in tasksList)
                    {

                        // delete task
                        restClient.deleteTask(task.task_id);

                        // run the task
                        TaskTools.runTask(task);
                        
                    }
                } // end if/else

            } // end if


        }



        // clear the logs
        private void nettoyerLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Log.clearLogFile();
        }


        // refresh Client
        private void rafraichirClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Log.add("Refresh Client", Log.level.info);

            // REST
            Rest restClient = new Rest(Config.serverURI);

            // get profiles List
            profilesList = restClient.getProfilesList(Config.username, Config.hostname);

            // check and put the first profile in currentProfile
            currentProfile = profilesList[0];
            ToolStripMenuItem firstItem = (ToolStripMenuItem)profilsToolStripMenuItem.DropDownItems[0];
            firstItem.Checked = true;

        }



        // Load ALL
        private void toutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Log.add("Load ALL (Shortcuts, printers, shares and options).", Log.level.info);
            
            // load shortcuts + taskbar
            currentProfile.loadDesktop(Config.whichDesktop, Config.deleteDesktopFiles, Config.deleteOnlyParxShortcuts, Config.cleanUserDesktop, Config.cleanPublicDesktop);

            System.Threading.Thread.Sleep(2000);

            // load Printers
            currentProfile.loadPrinters(Config.unmountPrinters);

            // load Shares
            currentProfile.loadShares(Config.unmountShares);

            // load Parameters
            Pipe.sendJsonToPipe("0", "loadCurrentProfileParameters", currentProfile.parameters);

        }



        // Update client
        private void miseAJourClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Pipe.sendJsonToPipe("0", "updateClient");

            var processInfo = new System.Diagnostics.ProcessStartInfo("cmd.exe", "/c ParxUpd.exe");
            processInfo.Verb = "runas";
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;

            var process = System.Diagnostics.Process.Start(processInfo);

        }



        // On context menu opening (shift modifier)
        private void contextMenu_Opening(object sender, CancelEventArgs e)
        {

            if (Control.ModifierKeys == Keys.Shift)
            {
                sauverToolStripMenuItem.Visible = true;
                configToolStripMenuItem.Visible = true;
                quitterToolStripMenuItem.Visible = true;
            }
            else
            {
                if (Config.disableMenus == true && !isAdmin)
                {
                    sauverToolStripMenuItem.Visible = false;
                    configToolStripMenuItem.Visible = false;
                    quitterToolStripMenuItem.Visible = false;
                }

            }

        }




        // On timer tick
        private void Timer_Tick(object sender, EventArgs e)
        {

            Log.add("Refresh tick", Log.level.debug);

            // refresh tasks list

            // rest client
            Rest restClient = new Rest(Config.serverURI);

            // get tasks list
            if (hostId != null)
            {
                tasksList = restClient.getTasksList(hostId);

                if (tasksList == null)
                {
                    Log.add("No task found.", Log.level.trivial);
                }
                else
                {
                    Log.add(tasksList.Count + " task(s) found.", Log.level.info);

                    // run tasks
                    foreach (Classes.Task task in tasksList)
                    {

                        // delete task
                        restClient.deleteTask(task.task_id);

                        // run the task
                        TaskTools.runTask(task);

                    }
                } // end if/else

            } // end if

        }


    }
}

