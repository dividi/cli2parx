﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Diagnostics;
using System.IO;

namespace ParxUpd
{
    class Program
    {

        static void ExecuteCommand(string command, bool verbose=false)
        {

            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            var process = Process.Start(processInfo);
            

            if (verbose == true)
            {
                process.OutputDataReceived += (object sender, DataReceivedEventArgs e) => Console.WriteLine("output>> " + e.Data);
                process.BeginOutputReadLine();

                process.ErrorDataReceived += (object sender, DataReceivedEventArgs e) => Console.WriteLine("error>> " + e.Data);
                process.BeginErrorReadLine();
            }
            
            process.WaitForExit();

            //Console.WriteLine("ExitCode: {0}", process.ExitCode);
            process.Close();
        }


        static void Help ()
        {
            Console.WriteLine();
            Console.WriteLine("Cli 2 Parx Updater Help");
            Console.WriteLine("-----------------------");

            Console.WriteLine("This application update the Cli2Parx client. You MUST run it with admin credentials.");

            Console.WriteLine("flags : ");
            Console.WriteLine("/? : help page");
            Console.WriteLine("/v : verbose mode");
        }




        static void updateClient (bool debugMode=false)
        {

            StreamWriter sw = new StreamWriter("c:\\cli2parx\\update.log", false, Encoding.UTF8);

            sw.WriteLine("Start : " + DateTime.Now.ToString("yyyyMMdd@HH:mm"));

            //Console.WriteLine("here2 : " + AppDomain.CurrentDomain.BaseDirectory);

            XDocument doc = XDocument.Load(Environment.CurrentDirectory + "\\config.xml");

            // config

            string serverURL = doc.Descendants("serverURL").Single<XElement>().Value;
            string clientVersion = doc.Descendants("version").Single<XElement>().Value;


            // info
            Console.WriteLine();
            Console.WriteLine("Welcome in Cli2Parx Updater");
            Console.WriteLine("---------------------------");
            Console.WriteLine();
            Console.WriteLine("Fetch current version : " + clientVersion);
            Console.WriteLine("Fetch server URL : " + serverURL);

            

            sw.WriteLine();
            sw.WriteLine("Welcome in Cli2Parx Updater");
            sw.WriteLine("---------------------------");
            sw.WriteLine();
            sw.WriteLine("Fetch current version : " + clientVersion);
            sw.WriteLine("Fetch server URL : " + serverURL);



            // shutdown Log viewer  
            try
            {
                Console.WriteLine("Shutdown LogViewer...");
                sw.WriteLine("Shutdown LogViewer...");

                foreach (Process p in Process.GetProcessesByName("LogViewer"))
                {
                    Console.WriteLine("Kill " + p.ProcessName + " !");
                    sw.WriteLine("Kill " + p.ProcessName + " !");
                    p.Kill();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with STOP LOGVIEWER block ! (" + ex.Message + ")");
                sw.WriteLine("Error with STOP LOGVIEWER block ! (" + ex.Message + ")");
            }



            // shutdown Client
            try
            {
                Console.WriteLine("Shutdown Client...");
                sw.WriteLine("Shutdown Client...");

                foreach (Process p in Process.GetProcessesByName("ParxCli"))
                {
                    Console.WriteLine("Kill " + p.ProcessName + " !");
                    sw.WriteLine("Kill " + p.ProcessName + " !");
                    p.Kill();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with STOP CLIENT block ! (" + ex.Message + ")");
                sw.WriteLine("Error with STOP CLIENT block ! (" + ex.Message + ")");
            }


            // stop service
            try
            {
                Console.WriteLine("Stop Cli2Parx service... ");
                sw.WriteLine("Stop Cli2Parx service... ");

                ExecuteCommand("nssm stop Cli2Parx", debugMode);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with STOP SERVICE block ! (" + ex.Message + ")");
                sw.WriteLine("Error with STOP SERVICE block ! (" + ex.Message + ")");
            }




            // remove service
            try
            {
                Console.WriteLine("Remove Cli2Parx service... ");
                sw.WriteLine("Remove Cli2Parx service... ");

                ExecuteCommand("nssm remove Cli2Parx confirm", debugMode);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with REMOVE CLI2PARX SERVICE block ! (" + ex.Message + ")");
                sw.WriteLine("Error with REMOVE CLI2PARX SERVICE block ! (" + ex.Message + ")");
            }











            // @TODO : Make use of DownloadFileCompleted to unzip when the DL is completed

            // Download update file
            try
            {
                Console.WriteLine("Download update file from server...");
                sw.WriteLine("Download update file from server...");
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    string updateFile = serverURL.Replace("index.php", @"files/parxshell.zip");
                    client.DownloadFile(updateFile, "c:\\cli2parx\\update.zip");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with DOWNLOAD block ! (" + ex.Message + ")");
                sw.WriteLine("Error with DOWNLOAD block ! (" + ex.Message + ")");
            }







            // unzip update file
            try
            {
                Console.WriteLine("Unzip file...");
                sw.WriteLine("Unzip file...");
                ExecuteCommand("7za x update.zip -aoa", debugMode);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with UNZIP block ! (" + ex.Message + ")");
                sw.WriteLine("Error with UNZIP block ! (" + ex.Message + ")");
            }




            // Clean up
            try
            {
                Console.WriteLine("Clean up...");
                sw.WriteLine("Clean up...");
                File.Delete("update.zip");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with CLEAN UP block ! (" + ex.Message + ")");
                sw.WriteLine("Error with CLEAN UP block ! (" + ex.Message + ")");
            }








            // service/client start and mount
            try
            {
                Console.WriteLine("Install Cli2Parx service... ");
                sw.WriteLine("Install Cli2Parx service... ");


                ExecuteCommand("nssm install Cli2Parx \"C:\\cli2parx\\ParxSrv.exe\"", debugMode);
                ExecuteCommand("nssm set Cli2Parx Description \"Service Parx\"", debugMode);
                ExecuteCommand("nssm set Cli2Parx Type SERVICE_INTERACTIVE_PROCESS", debugMode);
                ExecuteCommand("nssm set Cli2Parx AppStdout \"C:\\cli2parx\\service.log\"", debugMode);
                ExecuteCommand("nssm set Cli2Parx AppStderr \"C:\\cli2parx\\service.log\"", debugMode);

                Console.WriteLine("Start Cli2Parx service... ");
                sw.WriteLine("Start Cli2Parx service... ");
                ExecuteCommand("nssm start Cli2Parx", debugMode);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with INSTALL and START SERVICE block ! (" + ex.Message + ")");
                sw.WriteLine("Error with INSTALL and START SERVICE block ! (" + ex.Message + ")");
            }


            using (System.ServiceProcess.ServiceController service = new System.ServiceProcess.ServiceController("Cli2ParxUpd"))
            {
                if ( service.Status.Equals(System.ServiceProcess.ServiceControllerStatus.Running) )
                {
                    sw.WriteLine("Stop Cli2ParxUpd service");

                    service.Stop();
                   
                }
            }
            

            sw.Dispose();
            sw.Close();

        }



        static void Main(string[] args)
        {

            // if args
            if ( args.Length > 0 )
            {

                switch (args[0])
                {

                    case "/?":
                        Help();
                        break;

                    case "/v":
                        updateClient(true);
                        break;

                    default:
                        Help();
                        break;
                }

            }

            // no args
            else
            {
                updateClient();
            }
            




        }
    }





}
