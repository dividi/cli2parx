﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;


namespace LogViewer
{
    public partial class LogViewer : Form
    {
        public LogViewer()
        {
            InitializeComponent();
        }


        static string logFilePath = "";

        static DataTable data = new DataTable("logs");
                

        /// <summary>
        /// On File system watcher change
        /// </summary>
        private void OnChanged(object source, FileSystemEventArgs e)
        {

            if (logBox.InvokeRequired)
            {
                logBox.Invoke((MethodInvoker)delegate { OnChanged(source, e); });
            }
            else
            {
                FileStream fs = new FileStream(logFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                StreamReader reader = new StreamReader(fs);

                data.Clear();

                string line;
                int rowcount = 1;

                // read line per line
                while ((line = reader.ReadLine()) != null)
                {
                    AppendText(logBox, line, rowcount);
                    rowcount++;
                }


                reader.Close();

                // order
                logBox.Sort(logBox.Columns[0], ListSortDirection.Descending);

                setDataGridColor();
            }
        }




        // coloration
        private void setDataGridColor()
        {

            foreach (DataGridViewRow row in logBox.Rows)
            {

                Color color;
                Color backColor;

                switch ((string)row.Cells[2].Value)
                {
                    case "TRIVIAL": color = Color.Gray; backColor = Color.White; break;
                    case "ERROR": color = Color.White; backColor = Color.Red; break;
                    case "DEBUG": color = Color.DarkViolet; backColor = Color.White; break;
                    case "WARNING": color = Color.Pink; backColor = Color.White; break;
                    case "INFO": color = Color.Blue; backColor = Color.White; break;
                    case "TASK": color = Color.DarkGreen; backColor = Color.White; break;
                    case "PIPE": color = Color.AliceBlue; backColor = Color.White; break;
                    case "SERVICE": color = Color.AliceBlue; backColor = Color.White; break;
                    case "SRV_ERROR": color = Color.Red; backColor = Color.White; break;
                    case "SRV_INFO": color = Color.Blue; backColor = Color.White; break;
                    case "SRV_DEBUG": color = Color.DarkViolet; backColor = Color.White; break;
                    case "REG_ERROR": color = Color.DarkRed; backColor = Color.White; break;

                    default: color = Color.Black; backColor = Color.White; break;
                }
                
                // color
                row.DefaultCellStyle.ForeColor = color;
                row.DefaultCellStyle.BackColor = backColor;
            }

        }





        /// <summary>
        /// Add a line in the logviewer
        /// </summary>
        /// <param name="box">whitch richtextbox</param>
        /// <param name="text">line to write</param>
        private void AppendText(DataGridView box, string text, int index)
        {
            try
            {
                //20170116@15:53 [INFO       ] Clearing the Log file
                string date = text.Substring(0, 14);
                string type = text.Substring(16, 11).TrimEnd();
                string message = text.Substring(29, text.Length -29); // -29 = 14 for the date + 13 for the type + 2 spaces + 0 fuck I give

                // add
                DataRow dr = data.NewRow();
                dr[0] = index;
                dr[1] = date;
                dr[2] = type;
                dr[3] = message;

                data.Rows.Add(dr);
            }
            catch (Exception e)
            {
                Console.WriteLine("logviewer error : " + e);
            }
        }



        private void LogViewer_Load(object sender, EventArgs e)
        {

            Directory.SetCurrentDirectory(@"c:\cli2parx");
            
            // get the logfile in xml config
            XDocument doc = XDocument.Load(Environment.CurrentDirectory + "\\config.xml");
            logFilePath = doc.Descendants("logFile").Single<XElement>().Value;


            // set and bind the datasource
            data.Columns.Add("Index", typeof(int));
            data.Columns.Add("Date", typeof(string));
            data.Columns.Add("Type", typeof(string));
            data.Columns.Add("Message", typeof(string));

            logBox.DataSource = data;


            // log file path
            string logFolder = Path.GetDirectoryName(logFilePath) != "" ? Path.GetDirectoryName(logFilePath) : Application.StartupPath;
            string logFile = Path.GetFileName(logFilePath);


            // file watcher
            var watch = new FileSystemWatcher();

            watch.Path = logFolder;
            watch.Filter = logFile;
            watch.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite; //more options
            watch.Changed += new FileSystemEventHandler(OnChanged);
            watch.EnableRaisingEvents = true;


            // Initial log text
            StreamReader reader = new StreamReader(logFile);

            string line;
            int rowcount = 1;
            // read line per line
            while ((line = reader.ReadLine()) != null)
            {
                AppendText(logBox, line, rowcount);
                rowcount++;
            };
            reader.Close();

            // order
            logBox.Sort(logBox.Columns[0], ListSortDirection.Descending);

            setDataGridColor();
        }






        // wrap data
        private void wrapButton_Click(object sender, EventArgs e)
        {
            if (wrapButton.CheckState == CheckState.Checked)
            {
                logBox.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            }
            else
            {
                logBox.DefaultCellStyle.WrapMode = DataGridViewTriState.False;
            }
        }





        // filter table submit
        private void searchButton_Click(object sender, EventArgs e)
        {
            data.DefaultView.RowFilter = string.Format("Message LIKE '*{0}*'", searchBox.Text);
            setDataGridColor();
        }

        







    }





}
